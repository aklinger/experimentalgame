package experiment.code.map;

import java.util.Objects;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Toni
 */
public class CubeFace {
    //Attribute
    private Vector3f tilePosition;
    private Vector3f faceDirection;
    int blub = 3;
    //
    public static final Vector3f UP = new Vector3f(0,-1,0);
    public static final Vector3f DOWN = new Vector3f(0,1,0);
    public static final Vector3f LEFT = new Vector3f(-1,0,0);
    public static final Vector3f RIGHT = new Vector3f(1,0,0);
    public static final Vector3f BACK = new Vector3f(0,0,1);
    public static final Vector3f FRONT = new Vector3f(0,0,-1);
    public static final Vector3f[] DIRECTIONS = {UP,DOWN,LEFT,RIGHT,BACK,FRONT};
    //Konstruktor
    public CubeFace(Vector3f tilePosition, Vector3f faceDirection) {
        this.tilePosition = tilePosition;
        this.faceDirection = faceDirection;
    }
    //Methoden
    public Tile getTile(VoxelMap map){
        return map.getTile((int)(tilePosition.x), (int)(tilePosition.y), (int)(tilePosition.z));
    }
    public static Vector3f turnAround(Vector3f faceDirection, Vector3f moveDirection, boolean left){
        if(left){
            return Vector3f.cross(faceDirection,moveDirection,null);
        }else{
            return Vector3f.cross(moveDirection,faceDirection,null);
        }
    }
    public static boolean compareDirections(Vector3f v1, Vector3f v2){
        if((int)v1.x == (int)v2.x && (int)v1.y == (int)v2.y && (int)v1.z == (int)v2.z){
            return true;
        }
        return false;
    }
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof CubeFace)){
            return false;
        }
        CubeFace other = (CubeFace)obj;
        if((int)(this.tilePosition.x)==(int)(other.tilePosition.x)){
            if((int)(this.tilePosition.y)==(int)(other.tilePosition.y)){
                if((int)(this.tilePosition.z)==(int)(other.tilePosition.z)){
                    if(CubeFace.compareDirections(this.faceDirection,other.faceDirection)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (int)tilePosition.x;
        hash = 37 * hash + (int)tilePosition.y;
        hash = 37 * hash + (int)tilePosition.z;
        hash = 37 * hash + (int)faceDirection.x;
        hash = 37 * hash + (int)faceDirection.y;
        hash = 37 * hash + (int)faceDirection.z;
        return hash;
    }

    @Override
    public String toString() {
        return "CubeFace: ("+tilePosition.x +","+tilePosition.y +","+tilePosition.z+"),("+faceDirection.x+","+faceDirection.y+","+faceDirection.z+")";
    }
    //Getter & Setter
    public Vector3f getTilePosition() {
        return tilePosition;
    }

    public void setTilePosition(Vector3f tilePosition) {
        this.tilePosition = tilePosition;
    }

    public Vector3f getFaceDirection() {
        return faceDirection;
    }

    public void setFaceDirection(Vector3f faceDirection) {
        this.faceDirection = faceDirection;
    }
}
