package experiment.code.map;

import experiment.code.math.Vector3D;

/**
 * A basic 3D map data.
 * <p/>
 * @author Toni
 */
public class VoxelMap {
    //Attribute
    private Tile[][][] map;
    
    //Konstruktor
    public VoxelMap(int width, int height, int breadth) {
        map = new Tile[width][height][breadth];
    }
    
    //Methoden
    public Tile getTile(Vector3D position, double tileSize) {
        return getTile((int) (position.x() / tileSize), (int) (position.y() / tileSize), (int) (position.z() / tileSize));
    }

    public Tile getTile(int x, int y, int z) {
        if (x >= 0 && x < getXCount()) {
            if (y >= 0 && y < getYCount()) {
                if (z >= 0 && z < getZCount()) {
                    return map[x][y][z];
                }
            }
        }
        return null;
    }

    public void setTile(int x, int y, int z, Tile value) {
        map[x][y][z] = value;
    }
    
    //Getter & Setter
    public int getXCount() {
        return map.length;
    }

    public int getYCount() {
        return map[0].length;
    }

    public int getZCount() {
        return map[0][0].length;
    }
}
