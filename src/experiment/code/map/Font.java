package experiment.code.map;

import experiment.code.util.DalUtil;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;

/**
 * Used to write text without the need to load some complicated textures.
 * 
 * @author Toni
 */
public class Font {
    //Attribute
    private Map<String,int[][]> chars; 
    private int charWidth;
    private int charHeight;
    //Konstruktor
    public Font(int charWidth, int charHeight){
        chars = new HashMap<>();
        this.charWidth = charWidth;
        this.charHeight = charHeight;
    }
    //Methoden
    public static Font loadFontData(String pictureResource, String textResource) throws IOException{
        int charWidth = 5;
        int charHeight = 5;
        Font f = new Font(charWidth,charHeight);
        BufferedReader br = new BufferedReader(new InputStreamReader(DalUtil.asStream(textResource)));
        BufferedImage readImage = ImageIO.read(DalUtil.asStream(pictureResource));
        String text;
        int lineNumber = 0;
        while((text = br.readLine()) != null){
            for (int i = 0; i < text.length(); i++) {
                char nextChar = text.charAt(i);
                int[][] dots = new int[charWidth][charHeight];
                for (int j = 0; j < charWidth; j++) {
                    for (int k = 0; k < charHeight; k++) {
                        int rgb = readImage.getRGB(charWidth*i+j, charHeight*lineNumber+k);
                        dots[j][k] = (rgb & 1) == 1 ? 0 : 1;
                    }
                }
                f.putChar((""+nextChar).toLowerCase(), dots);
            }
            lineNumber++;
        }
        
        return f;
    }
    //Getter & Setter
    protected void putChar(String text, int[][] dots){
        chars.put(text, dots);
    }
    public int[][] getChar(String zeichen){
        int[][] result = chars.get(zeichen.toLowerCase());
        if(result == null){
            result = chars.get(" ");
        }
        return result;
    }

    public int getCharWidth() {
        return charWidth;
    }

    public int getCharHeight() {
        return charHeight;
    }
    
}
