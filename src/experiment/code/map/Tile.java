package experiment.code.map;

import java.awt.Color;

/**
 *
 * @author Toni
 */
public class Tile {
    //Static
    public static final int AIR = 0;
    public static final int GROUND = 1;
    public static final int SPIKES = 2;
    public static final int GAS = 3;
    //Attribute
    private boolean solid;
    private boolean lethal;
    private Color color;

    //Konstruktor
    private Tile(boolean solid, boolean lethal) {
        this.solid = solid;
        this.lethal = lethal;
        color = new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255));
    }

    public static Tile createTile(int type) {
        Tile t = null;
        switch (type) {
            case AIR:
                t = new Tile(false, false);
                break;
            case GROUND:
                t = new Tile(true, false);
                int range = 50;
                t.color = new Color((int) (Math.random() * range) + 100 - range / 2, (int) (Math.random() * range) + 150 - range / 2, (int) (Math.random() * range) + 200 - range / 2);
                break;
            case SPIKES:
                t = new Tile(true, true);
                t.color = new Color((int) (Math.random() * 50) + 200, (int) (Math.random() * 80) + 20, (int) (Math.random() * 20)+10);
                break;
            case GAS:
                t = new Tile(false, true);
                break;
            default:
                throw new RuntimeException("Unknown Tile-Type: " + type);
        }
        return t;
    }
    //Methoden

    //Getter & Setter
    public boolean isSolid() {
        return solid;
    }

    public void setSolid(boolean solid) {
        this.solid = solid;
    }

    public boolean isLethal() {
        return lethal;
    }

    public void setLethal(boolean lethal) {
        this.lethal = lethal;
    }

    public Color getColor() {
        return color;
    }
}
