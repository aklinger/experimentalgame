package experiment.code.map;

import experiment.code.math.Ray;
import experiment.code.util.ColorUtil;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

/**
 * A more concrete implementation of the 3D map data structure.
 * 
 * @author Toni
 */
public class CoolMap extends VoxelMap {
    //Attribute
    public float tileSize;
    
    //Konstruktor
    public CoolMap(int width, int height, int breadth, float tileSize) {
        super(width, height, breadth);
        this.tileSize = tileSize;
    }

    public static CoolMap createTextMap(float tileSize, Font font, String... text){
        int largest = 0;
        for (int i = 0; i < text.length; i++) {
            if(text[i].length() > largest){
                largest = text[i].length();
            }
        }
        CoolMap map = new CoolMap(1+largest*(font.getCharWidth()+1), 1+text.length*(font.getCharHeight()+1),2,tileSize);
        for (int i = 0; i < map.getXCount(); i++) {
            for (int j = 0; j < map.getYCount(); j++) {
                map.setTile(i, j, 1, Tile.createTile(Tile.GROUND));
            }
        }
        for (int i = 0; i < text.length; i++) {
            String tex = text[i];
            for (int j = 0; j < tex.length(); j++) {
                String zeichen = tex.substring(j, j+1);
                int[][] dots = font.getChar(zeichen);
                for (int k = 0; k < dots.length; k++) {
                    for (int l = 0; l < dots[0].length; l++) {
                        if(dots[k][l] == 1){
                            map.setTile(1+j*(font.getCharWidth()+1)+k, 1+i*(font.getCharHeight()+1)+l, 0, Tile.createTile(Tile.SPIKES));
                        }
                    }
                }
            }
        }
        return map;
    }
    
    public static CoolMap createCube(int mapSize, int cubeSize, float tileSize, double density) {
        CoolMap map = new CoolMap(mapSize, mapSize, mapSize, tileSize);
        for (int i = 0; i < cubeSize; i++) {
            for (int j = 0; j < cubeSize; j++) {
                for (int k = 0; k < cubeSize; k++) {
                    if (Math.random() < density) {
                        map.setTile(mapSize / 2 - cubeSize / 2 + i, mapSize / 2 - cubeSize / 2 + j, mapSize / 2 - cubeSize / 2 + k, Tile.createTile(Tile.GROUND));
                    }
                }
            }
        }
        return map;
    }
    
    public static CoolMap createCubeWithSolidCenter(int mapSize, int cubeSize, float tileSize, double density) {
        CoolMap map = new CoolMap(mapSize, mapSize, mapSize, tileSize);
        for (int i = 0; i < cubeSize; i++) {
            for (int j = 0; j < cubeSize; j++) {
                for (int k = 0; k < cubeSize; k++) {
                    if (Math.random() < density) {
                        map.setTile(mapSize / 2 - cubeSize / 2 + i, mapSize / 2 - cubeSize / 2 + j, mapSize / 2 - cubeSize / 2 + k, Tile.createTile(Tile.GROUND));
                    }
                }
            }
        }
        cubeSize -= 2;
        for (int i = 0; i < cubeSize; i++) {
            for (int j = 0; j < cubeSize; j++) {
                for (int k = 0; k < cubeSize; k++) {
                    map.setTile(mapSize / 2 - cubeSize / 2 + i, mapSize / 2 - cubeSize / 2 + j, mapSize / 2 - cubeSize / 2 + k, Tile.createTile(Tile.GROUND));
                }
            }
        }
        return map;
    }
    
    //Methoden
    public CubeFace getOppositeCubeFace(CubeFace position){
        Vector3f pos = new Vector3f(position.getTilePosition());
        Vector3f dir = position.getFaceDirection();
        while(isSolid(Vector3f.sub(pos,dir,null)) && getTile((int)(pos.x), (int)(pos.y), (int)(pos.z)) != null){
            Vector3f.sub(pos, dir, pos);
        }
        return new CubeFace(pos,dir.negate(null));
    }
    public CubeFace getLandingCubeFace(CubeFace position){
        Vector3f pos = new Vector3f(position.getTilePosition());
        Vector3f dir = position.getFaceDirection().negate(null);
        while(!isSolid(pos)){
            System.out.println("POS: "+new CubeFace(pos,dir.negate(null)));
            Vector3f.add(pos,dir,pos);
        }
        return new CubeFace(pos,dir.negate(null));
    }
    public Movement getNextCubeFaceInWalkDirection(Movement movement){
        return getNextCubeFaceInWalkDirection(movement.getPosition(), movement.getDirection());
    }
    public Movement getNextCubeFaceInWalkDirection(CubeFace position, Vector3f dir){
        Vector3f faceDir = position.getFaceDirection();
        Vector3f facePos = position.getTilePosition();
        float dot = Math.abs(Vector3f.dot(dir, faceDir));
        if(dot != 1){
            Vector3f pos = Vector3f.add(facePos,dir,null);
            Vector3f posUp = Vector3f.add(pos,faceDir,null);
            if(isSolid(posUp)){
                return new Movement(new CubeFace(posUp,dir.negate(null)),faceDir);
            }
            if(isSolid(pos)){
                return new Movement(new CubeFace(pos,faceDir),dir);
            }else{
                return new Movement(new CubeFace(facePos,dir),faceDir.negate(null));
            }
        }
        return null;
    }
    public List<CubeFace> getAdjacentCubeFaces(CubeFace face){
        List<CubeFace> list = new ArrayList<>(4);
        for (Vector3f dir : CubeFace.DIRECTIONS) {
            Movement neu = getNextCubeFaceInWalkDirection(face, dir);
            if(neu != null){
                list.add(neu.getPosition());
            }
        }
        return list;
    }
    public CubeFace getFacePickedByRay(Ray ray){
        float minDist = Float.MAX_VALUE;
        float currentDist;
        CubeFace hitFace = null;
        for (int x = 0; x < getXCount(); x++) {
            for (int y = 0; y < getYCount(); y++) {
                for (int z = 0; z < getZCount(); z++) {
                    if(isSolid(x,y,z)){
                        float l = (float)(x * tileSize);
                        float r = (float)(x * tileSize + tileSize);
                        float u = (float)(y * tileSize);
                        float d = (float)(y * tileSize + tileSize);
                        float f = (float)(z * tileSize);
                        float b = (float)(z * tileSize + tileSize);
                        if (!isSolid(x, y - 1, z)) {
                            //UpperSide
                            currentDist = Ray.rayIntersectsTriangle(ray, new Vector3f(l,u,f), new Vector3f(l,u,b), new Vector3f(r,u,f));
                            currentDist = Math.min(currentDist,Ray.rayIntersectsTriangle(ray, new Vector3f(r,u,f), new Vector3f(r,u,b), new Vector3f(l,u,b)));
                            if(currentDist < minDist){
                                minDist = currentDist;
                                hitFace = new CubeFace(new Vector3f(x,y,z),CubeFace.UP);
                            }
                        }
                        if (!isSolid(x, y + 1, z)) {
                            //BottomSide
                            currentDist = Ray.rayIntersectsTriangle(ray, new Vector3f(l,d,f), new Vector3f(l,d,b), new Vector3f(r,d,f));
                            currentDist = Math.min(currentDist,Ray.rayIntersectsTriangle(ray, new Vector3f(r,d,f), new Vector3f(r,d,b), new Vector3f(l,d,b)));
                            if(currentDist < minDist){
                                minDist = currentDist;
                                hitFace = new CubeFace(new Vector3f(x,y,z),CubeFace.DOWN);
                            }
                        }
                        if (!isSolid(x - 1, y, z)) {
                            //LeftSide
                            currentDist = Ray.rayIntersectsTriangle(ray, new Vector3f(l,u,f), new Vector3f(l,d,b), new Vector3f(l,d,f));
                            currentDist = Math.min(currentDist,Ray.rayIntersectsTriangle(ray, new Vector3f(l,u,f), new Vector3f(l,u,b), new Vector3f(l,d,b)));
                            if(currentDist < minDist){
                                minDist = currentDist;
                                hitFace = new CubeFace(new Vector3f(x,y,z),CubeFace.LEFT);
                            }
                        }
                        if (!isSolid(x + 1, y, z)) {
                            //RightSide
                            currentDist = Ray.rayIntersectsTriangle(ray, new Vector3f(r,u,f), new Vector3f(r,d,b), new Vector3f(r,d,f));
                            currentDist = Math.min(currentDist,Ray.rayIntersectsTriangle(ray, new Vector3f(r,u,f), new Vector3f(r,u,b), new Vector3f(r,d,b)));
                            if(currentDist < minDist){
                                minDist = currentDist;
                                hitFace = new CubeFace(new Vector3f(x,y,z),CubeFace.RIGHT);
                            }
                        }
                        if (!isSolid(x, y, z - 1)) {
                            //FrontSide
                            currentDist = Ray.rayIntersectsTriangle(ray, new Vector3f(l,u,f), new Vector3f(l,d,f), new Vector3f(r,u,f));
                            currentDist = Math.min(currentDist,Ray.rayIntersectsTriangle(ray, new Vector3f(r,u,f), new Vector3f(r,d,f), new Vector3f(l,d,f)));
                            if(currentDist < minDist){
                                minDist = currentDist;
                                hitFace = new CubeFace(new Vector3f(x,y,z),CubeFace.FRONT);
                            }
                        }
                        if (!isSolid(x, y, z + 1)) {
                            //Backside
                            currentDist = Ray.rayIntersectsTriangle(ray, new Vector3f(l,u,b), new Vector3f(l,d,b), new Vector3f(r,u,b));
                            currentDist = Math.min(currentDist,Ray.rayIntersectsTriangle(ray, new Vector3f(r,u,b), new Vector3f(r,d,b), new Vector3f(l,d,b)));
                            if(currentDist < minDist){
                                minDist = currentDist;
                                hitFace = new CubeFace(new Vector3f(x,y,z),CubeFace.BACK);
                            }
                        }
                    }
                }
            }
        }
        return hitFace;
    }
    public void translateMatrixToPosition(CubeFace position){
        Vector3f pos = position.getTilePosition();

        float bonusx = 0.5f;
        float bonusy = 0.5f;
        float bonusz = 0.5f;

        float xaxis = 0f;
        float zaxis = 0f;
        float degrees = 0f;
        Vector3f dir = position.getFaceDirection();
        if(CubeFace.compareDirections(CubeFace.UP, dir)){
            xaxis = 1f;
            degrees = 180;
            bonusy = 0;
        }else if(CubeFace.compareDirections(CubeFace.LEFT, dir)){
            zaxis = 1f;
            degrees = 90;
            bonusx = 0;
        }else if(CubeFace.compareDirections(CubeFace.RIGHT, dir)){
            zaxis = 1f;
            degrees = -90;
            bonusx = 1;
        }else if(CubeFace.compareDirections(CubeFace.FRONT, dir)){
            xaxis = 1f;
            degrees = -90;
            bonusz = 0;
        }else if(CubeFace.compareDirections(CubeFace.BACK, dir)){
            xaxis = 1f;
            degrees = 90;
            bonusz = 1;
        }else{
            bonusy = 1;
        }

        float transx = (pos.x+bonusx)*tileSize;
        float transy = (pos.y+bonusy)*tileSize;
        float transz = (pos.z+bonusz)*tileSize;

        GL11.glTranslatef(transx, transy, transz);

        if(degrees != 0){
            GL11.glRotatef(degrees, xaxis, 0, zaxis);
        }
    }

    
    
    public void renderFaceRing(CubeFace face, float inner, float outer){
        float o = 0.11f; //offset
        final float tShalf = tileSize/2f;
        Vector3f away = face.getFaceDirection();
        float xm = face.getTilePosition().x * tileSize + tShalf + away.x*(tShalf+o);
        float ym = face.getTilePosition().y * tileSize + tShalf + away.y*(tShalf+o);
        float zm = face.getTilePosition().z * tileSize + tShalf + away.z*(tShalf+o);
        Vector3f other1 = new Vector3f((away.y+away.z)*(tShalf*inner),(away.x+away.z)*(tShalf*inner),(away.x+away.y)*(tShalf*inner));
        Vector3f other2 = new Vector3f((away.y+away.z)*(tShalf*outer),(away.x+away.z)*(tShalf*outer),(away.x+away.y)*(tShalf*outer));
        int v = 1;
        if(other1.x == 0){
            v = -1;
        }
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glNormal3f(away.x, away.y, away.z);
        GL11.glVertex3f(xm+other1.x,ym+other1.y,zm+other1.z);
        GL11.glVertex3f(xm-other1.x,ym+other1.y,zm+other1.z*v);
        GL11.glVertex3f(xm-other2.x,ym+other2.y,zm+other2.z*v);
        GL11.glVertex3f(xm+other2.x,ym+other2.y,zm+other2.z);
        
        GL11.glVertex3f(xm-other1.x,ym+other1.y,zm+other1.z*v);
        GL11.glVertex3f(xm-other1.x,ym-other1.y,zm-other1.z);
        GL11.glVertex3f(xm-other2.x,ym-other2.y,zm-other2.z);
        GL11.glVertex3f(xm-other2.x,ym+other2.y,zm+other2.z*v);
        
        GL11.glVertex3f(xm-other1.x,ym-other1.y,zm-other1.z);
        GL11.glVertex3f(xm+other1.x,ym-other1.y,zm-other1.z*v);
        GL11.glVertex3f(xm+other2.x,ym-other2.y,zm-other2.z*v);
        GL11.glVertex3f(xm-other2.x,ym-other2.y,zm-other2.z);
        
        GL11.glVertex3f(xm+other1.x,ym+other1.y,zm+other1.z);
        GL11.glVertex3f(xm+other1.x,ym-other1.y,zm-other1.z*v);
        GL11.glVertex3f(xm+other2.x,ym-other2.y,zm-other2.z*v);
        GL11.glVertex3f(xm+other2.x,ym+other2.y,zm+other2.z);
        GL11.glEnd();
    }
    
    /*public void renderFace(CubeFace face, float grow){
        float o = 0.11f; //offset
        final float tShalf = tileSize/2f;
        Vector3f away = face.getFaceDirection();
        float xm = face.getTilePosition().x * tileSize + tShalf + away.x*(tShalf+o);
        float ym = face.getTilePosition().y * tileSize + tShalf + away.y*(tShalf+o);
        float zm = face.getTilePosition().z * tileSize + tShalf + away.z*(tShalf+o);
        Vector3f other = new Vector3f((away.y+away.z)*(tShalf*grow),(away.x+away.z)*(tShalf*grow),(away.x+away.y)*(tShalf*grow));
        int v = 1;
        if(other.x == 0){
            v = -1;
        }
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glNormal3f(away.x, away.y, away.z);
        GL11.glVertex3f(xm+other.x,ym+other.y,zm+other.z);
        GL11.glVertex3f(xm-other.x,ym+other.y,zm+other.z*v);
        GL11.glVertex3f(xm-other.x,ym-other.y,zm-other.z);
        GL11.glVertex3f(xm+other.x,ym-other.y,zm-other.z*v);
        GL11.glEnd();
    }*/
    
    public void renderFace(CubeFace face){
        renderFace(face,0f);
    }
    
    public void renderFace(CubeFace face, float grow){
        float o = 0.11f - grow; //offset
        int x = (int)(face.getTilePosition().x);
        int y = (int)(face.getTilePosition().y);
        int z = (int)(face.getTilePosition().z);
        float l = x * tileSize - grow;
        float r = x * tileSize + tileSize + grow;
        float u = y * tileSize - grow;
        float d = y * tileSize + tileSize + grow;
        float f = z * tileSize - grow;
        float b = z * tileSize + tileSize + grow;
        GL11.glBegin(GL11.GL_QUADS);
        if (CubeFace.compareDirections(CubeFace.UP,face.getFaceDirection())) {
            //UpperSide
            GL11.glNormal3f(0.0f, -1.0f, 0.0f);
            GL11.glVertex3d(l, u-o, f);
            GL11.glVertex3d(r, u-o, f);
            GL11.glVertex3d(r, u-o, b);
            GL11.glVertex3d(l, u-o, b);
        }
        if (CubeFace.compareDirections(CubeFace.FRONT,face.getFaceDirection())) {
            //FrontSide
            GL11.glNormal3f(0.0f, 0.0f, -1.0f);
            GL11.glVertex3d(l, u, f-o);
            GL11.glVertex3d(l, d, f-o);
            GL11.glVertex3d(r, d, f-o);
            GL11.glVertex3d(r, u, f-o);
        }
        if (CubeFace.compareDirections(CubeFace.LEFT,face.getFaceDirection())) {
            //LeftSide
            GL11.glNormal3f(-1.0f, 0.0f, 0.0f);
            GL11.glVertex3d(l-o, u, f);
            GL11.glVertex3d(l-o, d, f);
            GL11.glVertex3d(l-o, d, b);
            GL11.glVertex3d(l-o, u, b);
        }
        if (CubeFace.compareDirections(CubeFace.RIGHT,face.getFaceDirection())) {
            //RightSide
            GL11.glNormal3f(1.0f, 0.0f, 0.0f);
            GL11.glVertex3d(r+o, u, f);
            GL11.glVertex3d(r+o, d, f);
            GL11.glVertex3d(r+o, d, b);
            GL11.glVertex3d(r+o, u, b);
        }
        if (CubeFace.compareDirections(CubeFace.DOWN,face.getFaceDirection())) {
            //BottomSide
            GL11.glNormal3f(0.0f, 1.0f, 0.0f);
            GL11.glVertex3d(l, d+o, f);
            GL11.glVertex3d(r, d+o, f);
            GL11.glVertex3d(r, d+o, b);
            GL11.glVertex3d(l, d+o, b);
        }
        if (CubeFace.compareDirections(CubeFace.BACK,face.getFaceDirection())) {
            //Backside
            GL11.glNormal3f(0.0f, 0.0f, 1.0f);
            GL11.glVertex3d(l, u, b+o);
            GL11.glVertex3d(r, u, b+o);
            GL11.glVertex3d(r, d, b+o);
            GL11.glVertex3d(l, d, b+o);
        }
        GL11.glEnd();
    }
    public void renderCube(int x, int y, int z, double tileSize) {
        double l = x * tileSize;
        double r = x * tileSize + tileSize;
        double u = y * tileSize;
        double d = y * tileSize + tileSize;
        double f = z * tileSize;
        double b = z * tileSize + tileSize;
        ColorUtil.applyColor(getTile(x, y, z).getColor());
        GL11.glBegin(GL11.GL_QUADS);
        if (!isSolid(x, y - 1, z)) {
            //UpperSide
            GL11.glNormal3f(0.0f, -1.0f, 0.0f);
            GL11.glVertex3d(l, u, f);
            GL11.glVertex3d(r, u, f);
            GL11.glVertex3d(r, u, b);
            GL11.glVertex3d(l, u, b);
        }
        if (!isSolid(x, y, z - 1)) {
            //FrontSide
            GL11.glNormal3f(0.0f, 0.0f, -1.0f);
            GL11.glVertex3d(l, u, f);
            GL11.glVertex3d(l, d, f);
            GL11.glVertex3d(r, d, f);
            GL11.glVertex3d(r, u, f);
        }
        if (!isSolid(x - 1, y, z)) {
            //LeftSide
            GL11.glNormal3f(-1.0f, 0.0f, 0.0f);
            GL11.glVertex3d(l, u, f);
            GL11.glVertex3d(l, d, f);
            GL11.glVertex3d(l, d, b);
            GL11.glVertex3d(l, u, b);
        }
        if (!isSolid(x + 1, y, z)) {
            //RightSide
            GL11.glNormal3f(1.0f, 0.0f, 0.0f);
            GL11.glVertex3d(r, u, f);
            GL11.glVertex3d(r, d, f);
            GL11.glVertex3d(r, d, b);
            GL11.glVertex3d(r, u, b);
        }
        if (!isSolid(x, y + 1, z)) {
            //BottomSide
            GL11.glNormal3f(0.0f, 1.0f, 0.0f);
            GL11.glVertex3d(l, d, f);
            GL11.glVertex3d(r, d, f);
            GL11.glVertex3d(r, d, b);
            GL11.glVertex3d(l, d, b);
        }
        if (!isSolid(x, y, z + 1)) {
            //Backside
            GL11.glNormal3f(0.0f, 0.0f, 1.0f);
            GL11.glVertex3d(l, u, b);
            GL11.glVertex3d(r, u, b);
            GL11.glVertex3d(r, d, b);
            GL11.glVertex3d(l, d, b);
        }
        GL11.glEnd();
    }

    public void render() {
        //translate to the middle of the map
        GL11.glTranslated(-getXCount() * tileSize / 2, -getYCount() * tileSize / 2, -getZCount() * tileSize / 2);
        //render all the cubes
        for (int i = 0; i < getXCount(); i++) {
            for (int j = 0; j < getYCount(); j++) {
                for (int k = 0; k < getZCount(); k++) {
                    if (isSolid(i, j, k)) {
                        renderCube(i, j, k, tileSize);
                    }
                }
            }
        }
        //render a bounding-box of the map
        double l = 0;
        double r = getXCount() * tileSize;
        double u = 0;
        double d = getYCount() * tileSize;
        double f = 0;
        double b = getZCount() * tileSize;
        ColorUtil.applyColor(Color.WHITE);
        GL11.glBegin(GL11.GL_LINE_STRIP);

        GL11.glNormal3f(0, 0, 0);
        
        GL11.glVertex3d(l, u, b);
        GL11.glVertex3d(r, u, b);
        GL11.glVertex3d(r, d, b);
        GL11.glVertex3d(l, d, b);
        GL11.glVertex3d(l, u, b);

        GL11.glVertex3d(l, u, f);
        GL11.glVertex3d(r, u, f);
        GL11.glVertex3d(r, d, f);
        GL11.glVertex3d(l, d, f);
        GL11.glVertex3d(l, u, f);

        GL11.glVertex3d(l, d, f);
        GL11.glVertex3d(l, d, b);
        GL11.glVertex3d(r, d, b);
        GL11.glVertex3d(r, d, f);
        GL11.glVertex3d(r, u, f);
        GL11.glVertex3d(r, u, b);

        GL11.glEnd();
    }
    public boolean isSolid(Vector3f pos){
        return isSolid((int)pos.x,(int)pos.y,(int)pos.z);
    }
    public boolean isLethal(Vector3f pos){
        return isLethal((int)pos.x,(int)pos.y,(int)pos.z);
    }
    public boolean isSolid(int x, int y, int z) {
        return getTile(x, y, z) != null && getTile(x, y, z).isSolid();
    }

    public boolean isLethal(int x, int y, int z) {
        return getTile(x, y, z) != null && getTile(x, y, z).isLethal();
    }
}
