package experiment.code.map;

import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Toni
 */
public class Movement {
    //Attribute
    private CubeFace position;
    private Vector3f direction;
    
    //Konstruktor
    public Movement(CubeFace position, Vector3f direction) {
        this.position = position;
        this.direction = direction;
    }
    //Getter & Setter
    public CubeFace getPosition() {
        return position;
    }

    public void setPosition(CubeFace position) {
        this.position = position;
    }

    public Vector3f getDirection() {
        return direction;
    }

    public void setDirection(Vector3f direction) {
        this.direction = direction;
    }
}
