package experiment.code.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Toni
 */
public class Model {
    //Attribute
    public List<Vector3f> vertices;
    public List<Vector3f> normals;
    public List<Face> faces;
    private int displayList;
    //
    private static Map<String,Model> modelLibrary;
    static{
        modelLibrary = new HashMap<>();
    }
    //Konstruktor
    Model() {
        vertices = new ArrayList<>();
        normals = new ArrayList<>();
        faces = new ArrayList<>();
    }
    public static Model getModel(String name) throws IOException{
        Model m = modelLibrary.get(name);
        if(m == null){
            if(name.endsWith(".obj")){
                m = OBJLoader.loadModel(name);
                modelLibrary.put(name, m);
                m.createModelDisplayList();
            }else{
                throw new IOException("Format not known, currently only supports obj-files.");
            }
        }
        return m;
    }
    //Methoden
    public void createModelDisplayList() {
        displayList = GL11.glGenLists(1);
        GL11.glNewList(displayList, GL11.GL_COMPILE);
        {//Load the model
            for (Face face : faces) {
                GL11.glBegin(GL11.GL_POLYGON);
                for (int i = 0; i < face.vertex.size(); i++) {
                    Vector3f n = normals.get(face.normal.get(i) - 1);
                    GL11.glNormal3f(n.x, n.y, n.z);
                    Vector3f v = vertices.get(face.vertex.get(i) - 1);
                    GL11.glVertex3f(v.x, v.y, v.z);
                }
                GL11.glEnd();
            }
        }
        GL11.glEndList();
    }
    public void renderModel(){
        GL11.glCallList(displayList);
    }

    //Getter & Setter
    public int getDisplayList() {
        return displayList;
    }

    public void setDisplayList(int displayList) {
        this.displayList = displayList;
    }
}
