package experiment.code.model;

import experiment.code.map.CoolMap;
import experiment.code.map.CubeFace;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Toni
 */
public class Piece {
    //Attribute
    private Model model;
    private Vector3f scale;
    private float rotation;
    //Konstruktor
    public Piece(Model model, float scale) {
        this.model = model;
        this.scale = new Vector3f(scale,scale,scale);
    }
    //Methoden
    public void renderPiece(CubeFace position, Vector3f direction, CoolMap map){
        GL11.glPushMatrix();
        map.translateMatrixToPosition(position);
        GL11.glScalef(scale.x,scale.y,scale.z);
        GL11.glRotatef(rotation, 0, 1, 0);//TODO rotate according to direction
        model.renderModel();
        GL11.glPopMatrix();
    }
    public void setScale(float scale){
        this.scale.x = scale;
        this.scale.y = scale;
        this.scale.z = scale;
    }

    //Getter & Setter
    public void setScale(Vector3f scale) {
        this.scale = scale;
    }
    public float getAvgScale() {
        return (scale.x+scale.y+scale.z)/3;
    }
    public void setRotation(float rotation) {
        this.rotation = rotation;
    }
    public void addRotation(float rotation) {
        this.rotation += rotation;
    }
}
