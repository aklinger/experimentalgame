package experiment.code.model;

import experiment.code.util.DalUtil;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Toni
 */
public class OBJLoader {
    //Attribute
    
    //Konstruktor
    
    //Methoden
    public static Model loadModel(String resource) throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(DalUtil.asStream(resource)));
        Model m = new Model();
        String line;
        while ((line = reader.readLine()) != null){
            if(line.startsWith("v ")){
                String[] split = line.split(" ");
                float x = Float.valueOf(split[1]);
                float y = Float.valueOf(split[2]);
                float z = Float.valueOf(split[3]);
                m.vertices.add(new Vector3f(x,y,z));
            }else if(line.startsWith("vn ")){
                String[] split = line.split(" ");
                float x = Float.valueOf(split[1]);
                float y = Float.valueOf(split[2]);
                float z = Float.valueOf(split[3]);
                m.normals.add(new Vector3f(x,y,z));
            }else if (line.startsWith("f ")){
                String[] split = line.split(" ");
                Face face = new Face();
                for (int i = 1; i < split.length; i++) {
                    face.vertex.add(Integer.parseInt(split[i].split("/")[0]));
                    face.normal.add(Integer.parseInt(split[i].split("/")[2]));
                }
                m.faces.add(face);
            }
        }
        return m;
    }
}
