package experiment.code.logic.unit;

import experiment.code.logic.Manager;
import experiment.code.logic.Team;
import experiment.code.map.CoolMap;
import experiment.code.map.CubeFace;
import experiment.code.model.Piece;
import experiment.code.util.ColorUtil;
import java.util.Collection;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Toni
 */
public abstract class Unit {
    //Attribute
    private Piece piece;
    private CubeFace position;
    private Vector3f direction;
    private Team team;
    private int type;
    
    //Konstruktor
    public Unit(Piece piece, CubeFace position, Vector3f direction, Team team) {
        this.piece = piece;
        this.position = position;
        this.direction = direction;
        this.team = team;
    }
    public abstract Unit copy();
    //Methoden
    public void render(CoolMap map){
        GL11.glPushMatrix();
        ColorUtil.applyColor(team.getColor());
        piece.renderPiece(position, direction, map);
        GL11.glPopMatrix();
    }
    public abstract Collection<CubeFace> getValidMoveLocations(Manager manager);
    public void move(Manager manager, CubeFace destination){
        //NOOP
    }

    //Getter & Setter
    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public CubeFace getPosition() {
        return position;
    }

    public void setPosition(CubeFace position) {
        this.position = position;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
    
    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public Vector3f getDirection() {
        return direction;
    }
    
}
