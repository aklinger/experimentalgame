package experiment.code.logic;

import experiment.code.ai.AIThread;
import experiment.code.logic.unit.Unit;
import experiment.code.camera.QuaternionCamera;
import experiment.code.map.CoolMap;
import experiment.code.map.CubeFace;
import experiment.code.map.Font;
import experiment.code.map.Tile;
import java.io.IOException;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Toni
 */
public class ManagerFactory {
    //Attribute
    public static float tileSize = 2f;
    //Konstants
    public static final int CHESS = 1;
    public static final int SPLASH = 2;
    public static final int MENU = 3;
    public static final int TUTORIAL = 4;
    public static final int BLOCK = 5;
    public static final int SMALL = 6;
    public static final int CRAZY = 7;
    public static final int ARIMAA = 8;
    
    //Methoden
    public static Manager createManager(int mode, boolean aiEnabled) throws IOException{
        Manager man;
        Font font = Font.loadFontData("/res/font/zeichensatz_5x5pixel.png", "/res/font/charIndex");
        switch(mode){
            case CHESS:
                //Init Map
                CoolMap map = new CoolMap(10,3,10,tileSize);
                for (int i = 0; i < 8; i++) {
                    for (int j = 0; j < 8; j++) {
                        map.setTile(1+i, 1, 1+j, Tile.createTile(Tile.GROUND));
                    }
                }
                //Create Manager
                man = new Manager(map);
                //Init Teams
                man.getTeams().add(Team.TEAM_WHITE);
                man.getTeams().add(Team.TEAM_GRAY);
                //Init AI
                if(aiEnabled){
                    man.getAis().add(new AIThread(man,Team.TEAM_GRAY,2,10*1000));
                }
                //Init Pieces
                for (int k = -1; k <= 1; k += 2) {
                    int team = k == -1 ? 0 : 1;
                    Team currentTeam = man.getTeams().get(team);
                    Vector3f dir = new Vector3f(0,0,-k);
                    for (int i = 0; i < 8; i++) {
                        CubeFace position = new CubeFace(new Vector3f(1+i,1,4.5f+(2.5f*k)),CubeFace.UP);
                        Unit unit;
                        unit = UnitFactory.createUnit(UnitFactory.PAWN, position,dir,currentTeam);
                        man.addUnit(unit);
                        
                        position = new CubeFace(new Vector3f(1+i,1,4.5f+(3.5f*k)),CubeFace.UP);
                        if(i==0 || i==7){
                            unit = UnitFactory.createUnit(UnitFactory.TOWER,position,dir,currentTeam);
                        }else if(i==1 || i==6){
                            unit = UnitFactory.createUnit(UnitFactory.KNIGHT,position,dir,currentTeam);
                        }else if(i==2 || i==5){
                            unit = UnitFactory.createUnit(UnitFactory.BISHOP,position,dir,currentTeam);
                        }else if(i==3){
                            unit = UnitFactory.createUnit(UnitFactory.QUEEN,position,dir,currentTeam);
                        }else{
                            unit = UnitFactory.createUnit(UnitFactory.KING,position,dir,currentTeam);
                        }
                        man.addUnit(unit);
                    }
                }
                //Init Camera
                QuaternionCamera cam = new QuaternionCamera();
                cam.setZoomLimits(0.1f, 10f);
                cam.addTranslation(new Vector3f(0, 0, -50));
                cam.rotateQuaternionCamera(new Vector3f(1,0,0), 45);
                cam.updateQuaternionCamera();
                man.setCamera(cam);
                
                break;
            case ARIMAA:
                //Init Map
                map = new CoolMap(10,3,10,tileSize);
                for (int i = 0; i < 8; i++) {
                    for (int j = 0; j < 8; j++) {
                        if((i == 2 || i == 5) && (j == 2 || j == 5)){
                            map.setTile(1+i, 1, 1+j, Tile.createTile(Tile.SPIKES));
                        }else{
                            map.setTile(1+i, 1, 1+j, Tile.createTile(Tile.GROUND));
                        }
                    }
                }
                //Create Manager
                man = new Manager(map);
                man.setMovesPerTurn(4);
                //Init Teams
                man.getTeams().add(Team.TEAM_YELLOW);
                man.getTeams().add(Team.TEAM_GRAY);
                //Init AI
                if(aiEnabled){
                    man.getAis().add(new AIThread(man,Team.TEAM_GRAY,2,10*1000));
                }
                //Init Pieces
                for (int k = -1; k <= 1; k += 2) {
                    int team = k == -1 ? 0 : 1;
                    Team currentTeam = man.getTeams().get(team);
                    Vector3f dir = new Vector3f(0,0,-k);
                    for (int i = 0; i < 8; i++) {
                        CubeFace position = new CubeFace(new Vector3f(1+i,1,4.5f+(2.5f*k)),CubeFace.UP);
                        Unit unit = UnitFactory.createUnit(UnitFactory.ARIMAA_UNIT, position,dir,currentTeam);
                        man.addUnit(unit);
                        
                        position = new CubeFace(new Vector3f(1+i,1,4.5f+(3.5f*k)),CubeFace.UP);
                        if(i==0 || i==7){
                            unit = UnitFactory.createUnit(UnitFactory.ARIMAA_UNIT,position,dir,currentTeam);
                        }else if(i==1 || i==6){
                            unit = UnitFactory.createUnit(UnitFactory.ARIMAA_UNIT,position,dir,currentTeam);
                        }else if(i==2 || i==5){
                            unit = UnitFactory.createUnit(UnitFactory.ARIMAA_UNIT,position,dir,currentTeam);
                        }else if(i==3){
                            unit = UnitFactory.createUnit(UnitFactory.ARIMAA_UNIT,position,dir,currentTeam);
                        }else{
                            unit = UnitFactory.createUnit(UnitFactory.ARIMAA_UNIT,position,dir,currentTeam);
                        }
                        man.addUnit(unit);
                    }
                }
                //Init Camera
                cam = new QuaternionCamera();
                cam.setZoomLimits(0.1f, 10f);
                cam.addTranslation(new Vector3f(0, 0, -50));
                cam.rotateQuaternionCamera(new Vector3f(1,0,0), 45);
                cam.updateQuaternionCamera();
                man.setCamera(cam);
                
                break;
           case CRAZY:
                //Init Map
               int mapWidth = 8;
                map = CoolMap.createCubeWithSolidCenter(10, mapWidth, tileSize, 0.5f);
                
                //Create Manager
                man = new Manager(map);
                //Init Teams
                man.getTeams().add(Team.TEAM_RED);
                man.getTeams().add(Team.TEAM_YELLOW);
                //Init AI
                if(aiEnabled){
                    man.getAis().add(new AIThread(man,Team.TEAM_YELLOW,2,10*1000));
                }
                //Init Pieces
                for (int k = -1; k <= 1; k += 2) {
                    int team = k == -1 ? 0 : 1;
                    Team currentTeam = man.getTeams().get(team);
                    Vector3f dir = new Vector3f(0,0,-k);
                    Vector3f bottom = new Vector3f(0,k,0);
                    int yPos = 1;
                    if(k>0){
                        yPos = mapWidth+1;
                    }
                    for (int i = 0; i < mapWidth-2; i++) {
                        CubeFace position = new CubeFace(new Vector3f(2+i,yPos,((float)(mapWidth+1)/2f)+(1.5f*k)),bottom);
                        position = map.getLandingCubeFace(position);
                        Unit unit;
                        unit = UnitFactory.createUnit(UnitFactory.DRILL, position,dir,currentTeam);
                        man.addUnit(unit);
                        
                        position = new CubeFace(new Vector3f(2+i,yPos,((float)(mapWidth+1)/2f)+(2.5f*k)),bottom);
                        position = map.getLandingCubeFace(position);
                        
                        if(i==0 || i==(mapWidth-2)-1){
                            unit = UnitFactory.createUnit(UnitFactory.SWEEPER,position,dir,currentTeam);
                        }else if(i==1 || i==(mapWidth-2)-2){
                            unit = UnitFactory.createUnit(UnitFactory.JUMPER,position,dir,currentTeam);
                        }else if(i==2 || i==(mapWidth-2)-3){
                            unit = UnitFactory.createUnit(UnitFactory.PEASANT,position,dir,currentTeam);//Add ROCK
                        }else{
                            unit = UnitFactory.createUnit(UnitFactory.PEASANT,position,dir,currentTeam);
                        }
                        man.addUnit(unit);
                    }
                }
                //Init Camera
                cam = new QuaternionCamera();
                cam.setZoomLimits(0.1f, 10f);
                cam.addTranslation(new Vector3f(0, 0, -50));
                cam.rotateQuaternionCamera(new Vector3f(0,1,0), (float)Math.toRadians(45));
                cam.rotateQuaternionCamera(new Vector3f(1,0,0), (float)Math.toRadians(45));
                cam.updateQuaternionCamera();
                man.setCamera(cam);
                break;
           case BLOCK:
                //Init Map
                mapWidth = 8;
                map = CoolMap.createCubeWithSolidCenter(10, mapWidth, tileSize, 0.1f);
                
                //Create Manager
                man = new Manager(map);
                //Init Teams
                man.getTeams().add(Team.TEAM_RED);
                man.getTeams().add(Team.TEAM_YELLOW);
                //Init AI
                if(aiEnabled){
                    man.getAis().add(new AIThread(man,Team.TEAM_YELLOW,2,10*1000));
                }
                //Init Pieces
                for (int k = -1; k <= 1; k += 2) {
                    int team = k == -1 ? 0 : 1;
                    Team currentTeam = man.getTeams().get(team);
                    Vector3f dir = new Vector3f(0,0,-k);
                    Vector3f bottom = new Vector3f(0,k,0);
                    int yPos = 1;
                    if(k>0){
                        yPos = mapWidth+1;
                    }
                    for (int i = 0; i < mapWidth-2; i++) {
                        CubeFace position = new CubeFace(new Vector3f(2+i,yPos,((float)(mapWidth+1)/2f)+(1.5f*k)),bottom);
                        position = map.getLandingCubeFace(position);
                        Unit unit;
                        unit = UnitFactory.createUnit(UnitFactory.DRILL, position,dir,currentTeam);
                        man.addUnit(unit);
                        
                        position = new CubeFace(new Vector3f(2+i,yPos,((float)(mapWidth+1)/2f)+(2.5f*k)),bottom);
                        position = map.getLandingCubeFace(position);
                        
                        if(i==0 || i==(mapWidth-2)-1){
                            unit = UnitFactory.createUnit(UnitFactory.SWEEPER,position,dir,currentTeam);
                        }else if(i==1 || i==(mapWidth-2)-2){
                            unit = UnitFactory.createUnit(UnitFactory.JUMPER,position,dir,currentTeam);
                        }else if(i==2 || i==(mapWidth-2)-3){
                            unit = UnitFactory.createUnit(UnitFactory.PEASANT,position,dir,currentTeam);//Add ROCK
                        }else{
                            unit = UnitFactory.createUnit(UnitFactory.PEASANT,position,dir,currentTeam);
                        }
                        man.addUnit(unit);
                    }
                }
                //Init Camera
                cam = new QuaternionCamera();
                cam.setZoomLimits(0.1f, 10f);
                cam.addTranslation(new Vector3f(0, 0, -50));
                cam.rotateQuaternionCamera(new Vector3f(0,1,0), (float)Math.toRadians(45));
                cam.rotateQuaternionCamera(new Vector3f(1,0,0), (float)Math.toRadians(5));
                cam.updateQuaternionCamera();
                man.setCamera(cam);
                break;
           case SMALL:
                //Init Map
                mapWidth = 6;
                map = CoolMap.createCubeWithSolidCenter(mapWidth+2, mapWidth, tileSize, 0.15f);
                
                //Create Manager
                man = new Manager(map);
                //Init Teams
                man.getTeams().add(Team.TEAM_RED);
                man.getTeams().add(Team.TEAM_YELLOW);
                //Init AI
                if(aiEnabled){
                    man.getAis().add(new AIThread(man,Team.TEAM_YELLOW,2,10*1000));
                }
                //Init Pieces
                for (int k = -1; k <= 1; k += 2) {
                    int team = k == -1 ? 0 : 1;
                    Team currentTeam = man.getTeams().get(team);
                    Vector3f dir = new Vector3f(0,0,-k);
                    Vector3f bottom = new Vector3f(0,k,0);
                    int yPos = 1;
                    if(k>0){
                        yPos = mapWidth+1;
                    }
                    for (int i = 0; i < mapWidth-2; i++) {
                        CubeFace position = new CubeFace(new Vector3f(2+i,yPos,((float)(mapWidth+1)/2f)+(0.5f*k)),bottom);
                        position = map.getLandingCubeFace(position);
                        Unit unit;
                        unit = UnitFactory.createUnit(UnitFactory.DRILL, position,dir,currentTeam);
                        man.addUnit(unit);
                        
                        position = new CubeFace(new Vector3f(2+i,yPos,((float)(mapWidth+1)/2f)+(1.5f*k)),bottom);
                        position = map.getLandingCubeFace(position);
                        
                        if(i==0 || i==(mapWidth-2)-1){
                            unit = UnitFactory.createUnit(UnitFactory.SWEEPER,position,dir,currentTeam);
                        }else if(i==1 || i==(mapWidth-2)-2){
                            unit = UnitFactory.createUnit(UnitFactory.JUMPER,position,dir,currentTeam);
                        }else if(i==2 || i==(mapWidth-2)-3){
                            unit = UnitFactory.createUnit(UnitFactory.PEASANT,position,dir,currentTeam);//Add ROCK
                        }else{
                            unit = UnitFactory.createUnit(UnitFactory.PEASANT,position,dir,currentTeam);
                        }
                        man.addUnit(unit);
                    }
                }
                //Init Camera
                cam = new QuaternionCamera();
                cam.setZoomLimits(0.1f, 10f);
                cam.addTranslation(new Vector3f(0, 0, -50));
                cam.rotateQuaternionCamera(new Vector3f(0,1,0), (float)Math.toRadians(45));
                cam.rotateQuaternionCamera(new Vector3f(1,0,0), (float)Math.toRadians(5));
                cam.updateQuaternionCamera();
                man.setCamera(cam);
                break;
            case SPLASH:
                //Init Map
                String[] info = new String[]{" "," Hello and Welcome to", " 3D chess and more crazy stuff!", 
                    " ", "You can use the", 
                    "right mouse button to", "rotate the view.",
                    "Use the Spacebar or Middle Mouse", "to pan the view. Try it out!",
                    "Don't forget the Mouse Wheel,", "because you can zoom with it.",
                    " ","To continue simply press 1.", " "};
                map = CoolMap.createTextMap(0.5f, font, info);
                //Create Manager
                man = new Manager(map);
                //Init Camera
                cam = new QuaternionCamera();
                cam.setZoomLimits(0.1f, 10f);
                cam.setZoom(0.5f);
                cam.addTranslation(new Vector3f(0.25f, 0.20f, -50));
                cam.rotateQuaternionCamera(new Vector3f(1,0,0), (float)Math.toRadians(-15));
                cam.rotateQuaternionCamera(new Vector3f(0,1,0), (float)Math.toRadians(15));
                cam.updateQuaternionCamera();
                man.setCamera(cam);
                
                break;
            case MENU:
                //Init Map
                info = new String[]{"Press \"T\" for a text introduction.",
                                    " ",
                                    "Or Press 2, 3, 4 or 5","to simply try it out ","and play against one of your friends.",
                                    " ",
                                    "Got no Friends?","Press 6, 7, 8 or 9", "to play against a crappy AI.",
                                    " ",
                                    "Number 2 and 6 are on a smaller board", "perfect for beginners who want to learn.",
                                    " ",
                                    "Want to play chess?","Press 5 or 9","to try out a 'new' inbalanced chess.",
                                    " ",
                                    "Hold ESC to exit."};
                map = CoolMap.createTextMap(0.5f, font, info);
                //Create Manager
                man = new Manager(map);
                //Init Camera
                cam = new QuaternionCamera();
                cam.setZoomLimits(0.1f, 10f);
                cam.setZoom(0.5f);
                cam.addTranslation(new Vector3f(0.25f, 0.20f, -50));
                cam.rotateQuaternionCamera(new Vector3f(1,0,0), (float)Math.toRadians(-15));
                cam.rotateQuaternionCamera(new Vector3f(0,1,0), (float)Math.toRadians(15));
                cam.updateQuaternionCamera();
                man.setCamera(cam);
                
                break;
            case TUTORIAL:
                //Init Map
                info = new String[]{" Hello here is how to play", " 3D chess and more crazy stuff!", 
                    " ", "This is a turn-based strategy game.",
                    "If you know chess you will ","probably understand how to play.",
                    " ",
                    "When you select a unit, ","it's valid move locations are highlighted.",
                    " ",
                    "Keep an eye out for unusual move patterns","like the digger, who can simply","pop out at the other side of the playfield",
                    " ",
                    "Then you click where you want it to move,","and then it's the other player's turn.",
                    " ",
                    "When you move to a field occupied by an enemy,","you destroy the enemy unit.",
                    " ",
                    "You win by feeling happy.",
                    " ",
                    "Press 1 to go back to the Main Menu."};
                map = CoolMap.createTextMap(0.5f, font, info);
                //Create Manager
                man = new Manager(map);
                //Init Camera
                cam = new QuaternionCamera();
                cam.setZoomLimits(0.1f, 10f);
                cam.setZoom(0.5f);
                cam.addTranslation(new Vector3f(5.0f, 1.5f, -50));
                cam.rotateQuaternionCamera(new Vector3f(1,0,0), (float)Math.toRadians(-15));
                cam.rotateQuaternionCamera(new Vector3f(0,1,0), (float)Math.toRadians(15));
                cam.updateQuaternionCamera();
                man.setCamera(cam);
                
                break;
            default:
                man = null;
                break;
        }
        return man;
    }
}
