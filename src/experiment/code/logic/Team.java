package experiment.code.logic;

import java.awt.Color;

/**
 *
 * @author Toni
 */
public class Team {
    //Attribute
    private int id;
    private String name;
    private Color color;
    //
    public static final Team TEAM_RED = new Team(1,"Team Red",new Color(250,100,100));
    public static final Team TEAM_GREEN = new Team(2,"Team Green",new Color(100,250,100));
    public static final Team TEAM_BLUE = new Team(3,"Team Blue",new Color(100,100,250));
    public static final Team TEAM_YELLOW = new Team(4,"Team Yellow",new Color(250,250,100));
    public static final Team TEAM_CYAN = new Team(5,"Team Cyan",new Color(100,250,250));
    public static final Team TEAM_MAGENTA = new Team(6,"Team Magenta",new Color(250,100,250));
    public static final Team TEAM_WHITE = new Team(7,"Team White",new Color(250,250,250));
    public static final Team TEAM_GRAY = new Team(8,"Team Gray",new Color(100,100,100));
    //Konstruktor
    public Team(int id, String name, Color color) {
        this.id = id;
        this.name = name;
        this.color = color;
    }
    //Getter & Setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
