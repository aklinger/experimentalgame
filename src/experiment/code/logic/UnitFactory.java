package experiment.code.logic;

import experiment.code.logic.unit.Unit;
import experiment.code.map.CubeFace;
import experiment.code.map.Movement;
import experiment.code.model.Model;
import experiment.code.model.Piece;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Toni
 */
public class UnitFactory {
    //Attribute
    public static final int KING = 1;
    public static final int PAWN = 2;
    public static final int TOWER = 3;
    public static final int BISHOP = 4;
    public static final int KNIGHT = 5;
    public static final int QUEEN = 6;
    public static final int DRILL = 7;
    public static final int JUMPER = 8;
    public static final int PEASANT = 9;
    public static final int SWEEPER = 10;
    public static final int ROCK = 11;//TODO instead of clinging to walls falls straight down
    public static final int ARIMAA_UNIT = 12;
    //Methoden
    public static Unit createUnit(int type, CubeFace position, Vector3f direction, Team team) throws IOException{
        Unit unit;
        Piece p;
        switch(type){
            case KING:
                p = new Piece(Model.getModel("/res/models/chess/chess_piece1.obj"),0.38f);
                QueenMover king = new QueenMover(p,position,direction,team);
                king.range = 1;
                unit = king;
                break;
            case PAWN:
                p = new Piece(Model.getModel("/res/models/chess/chess_piece7.obj"),0.30f);
                PawnUnit pUnit = new PawnUnit(p,position,direction,team);
                pUnit.walkDirection = direction;
                unit = pUnit;
                break;
            case TOWER:
                p = new Piece(Model.getModel("/res/models/chess/chess_piece2.obj"),0.5f);
                LineMover lUnit = new LineMover(p,position,direction,team);
                lUnit.range = 50;
                unit = lUnit;
                break;
            case BISHOP:
                p = new Piece(Model.getModel("/res/models/chess/chess_piece4.obj"),0.42f);
                DiagonalMover dMover = new DiagonalMover(p,position,direction,team);
                dMover.range = 50;
                unit = dMover;
                break;
            case KNIGHT:
                p = new Piece(Model.getModel("/res/models/chess/chess_piece3.obj"),0.35f);
                JumpingMover jumper = new JumpingMover(p,position,direction,team);
                jumper.ranges = new int[]{1,2};
                jumper.directionChanges = 1;
                unit = jumper;
                break;
            case QUEEN:
                p = new Piece(Model.getModel("/res/models/chess/chess_piece6.obj"),0.4f);
                QueenMover queen = new QueenMover(p,position,direction,team);
                queen.range = 50;
                unit = queen;
                break;
            case JUMPER:
                p = new Piece(Model.getModel("/res/models/chess/chess_piece3.obj"),0.35f);
                jumper = new JumpingMover(p,position,direction,team);
                jumper.ranges = new int[]{0,1,2};
                jumper.directionChanges = 2;
                unit = jumper;
                break;
            case ARIMAA_UNIT:
                /*p = new Piece(Model.getModel("/res/models/chess/chess_piece1.obj"),0.38f);
                ArimaaUnit aUnit = new ArimaaUnit(p,position,direction,team);
                aUnit.strength = 1;
                unit = aUnit;
                break;*/
            case DRILL:
                p = new Piece(Model.getModel("/res/models/chess/chess_piece5.obj"),0.4f);
                DrillMover dUnit = new DrillMover(p,position,direction,team);
                dUnit.range = 1;
                unit = dUnit;
                break;
            case PEASANT:
                p = new Piece(Model.getModel("/res/models/chess/chess_piece4.obj"),0.3f);
                DiagonalMover peasant = new DiagonalMover(p,position,direction,team);
                peasant.range = 5;
                unit = peasant;
                break;
            case SWEEPER:
                p = new Piece(Model.getModel("/res/models/chess/chess_piece1.obj"),0.4f);
                NormalUnit nUnit = new NormalUnit(p,position,direction,team);
                nUnit.range = 3;
                unit = nUnit;
                break;
            
            default:
                unit = null;
                break;
        }
        if(unit != null){
            unit.setType(type);
        }
        return unit;
    }
    //Inner Classes
    private static class DrillMover extends NormalUnit{
        //Konstruktor
        public DrillMover(Piece piece, CubeFace position, Vector3f direction, Team team) {
            super(piece, position, direction, team);
        }
        
        @Override
        public Collection<CubeFace> getValidMoveLocations(Manager manager) {
            Collection<CubeFace> finalFaces = super.getValidMoveLocations(manager);
            CubeFace cubeFace = manager.getMap().getOppositeCubeFace(this.getPosition());
            Unit other = manager.getUnitAt(cubeFace);
            if(other == null || other.getTeam() != getTeam() ){
                finalFaces.add(cubeFace);
            }
            return finalFaces;
        }

        @Override
        public Unit copy() {
            return new DrillMover(getPiece(), getPosition(), getDirection(), getTeam());
        }
    }
    private static class LineMover extends Unit{
        //Attribute
        protected int range;
        //Konstruktor
        public LineMover(Piece piece, CubeFace position, Vector3f direction, Team team) {
            super(piece, position, direction, team);
        }
        @Override
        public Unit copy() {
            LineMover unit = new LineMover(getPiece(), getPosition(), getDirection(), getTeam());
            unit.range = range;
            return unit;
        }
        @Override
        public Collection<CubeFace> getValidMoveLocations(Manager manager) {
            Collection<CubeFace> finalFaces = new ArrayDeque<>();
            for (int j = 0; j < CubeFace.DIRECTIONS.length; j++) {
                CubeFace currentFace = this.getPosition();
                Vector3f currDir = CubeFace.DIRECTIONS[j];
                for (int i = 0; i < range; i++) {
                    Movement movement = manager.getMap().getNextCubeFaceInWalkDirection(currentFace,currDir);
                    if(movement != null){
                        CubeFace neuFace = movement.getPosition();
                        currDir = movement.getDirection();
                        Unit other = manager.getUnitAt(neuFace);
                        if(other != null){
                            if(other.getTeam() != getTeam()){
                                finalFaces.add(neuFace);
                            }
                            break;
                        }else if(!finalFaces.contains(neuFace)){
                            finalFaces.add(neuFace);
                        }
                        currentFace = neuFace;
                    }else{
                        break;
                    }
                }
            }
            for (Iterator<CubeFace> it = finalFaces.iterator(); it.hasNext();) {
                CubeFace cubeFace = it.next();
                Unit other = manager.getUnitAt(cubeFace);
                if(other != null && other.getTeam() == getTeam() ){
                    it.remove();
                }
            }
            return finalFaces;
        }
    }
    private static class DiagonalMover extends Unit{
        //Attribute
        protected int range;
        //Konstruktor
        public DiagonalMover(Piece piece, CubeFace position, Vector3f direction, Team team) {
            super(piece, position, direction, team);
        }
        @Override
        public Unit copy() {
            DiagonalMover unit = new DiagonalMover(getPiece(), getPosition(), getDirection(), getTeam());
            unit.range = range;
            return unit;
        }
        @Override
        public Collection<CubeFace> getValidMoveLocations(Manager manager) {
            Collection<CubeFace> finalFaces = new ArrayDeque<>();
            for (int j = 0; j < CubeFace.DIRECTIONS.length; j++) {
                for (int twice = 0; twice < 2; twice++){
                    CubeFace currentFace = this.getPosition();
                    Vector3f currDir = CubeFace.DIRECTIONS[j];
                    boolean left = twice%2 == 0;
                    for (int i = 0; i < range*2; i++) {
                        Movement movement = manager.getMap().getNextCubeFaceInWalkDirection(currentFace,currDir);
                        if(movement != null){
                            CubeFace neuFace = movement.getPosition();
                            currDir = movement.getDirection();
                            if(i % 2 == 1){
                                Unit other = manager.getUnitAt(neuFace);
                                if(other != null){
                                    if(other.getTeam() != getTeam()){
                                        finalFaces.add(neuFace);
                                    }
                                    break;
                                }else if(!finalFaces.contains(neuFace)){
                                    finalFaces.add(neuFace);
                                }
                            }
                            currentFace = neuFace;
                            currDir = CubeFace.turnAround(currentFace.getFaceDirection(),currDir,left);
                            left = ! left;
                        }else{
                            break;
                        }
                    }
                }
            }
            for (Iterator<CubeFace> it = finalFaces.iterator(); it.hasNext();) {
                CubeFace cubeFace = it.next();
                Unit other = manager.getUnitAt(cubeFace);
                if(other != null && other.getTeam() == getTeam() ){
                    it.remove();
                }
            }
            return finalFaces;
        }
    }
    private static class QueenMover extends Unit{
        //Attribute
        protected int range;
        //Konstruktor
        public QueenMover(Piece piece, CubeFace position, Vector3f direction, Team team) {
            super(piece, position, direction, team);
        }
        
        @Override
        public Unit copy() {
            QueenMover unit = new QueenMover(getPiece(), getPosition(), getDirection(), getTeam());
            unit.range = range;
            return unit;
        }
        @Override
        public Collection<CubeFace> getValidMoveLocations(Manager manager) {
            Collection<CubeFace> finalFaces = new ArrayDeque<>();
            for (int j = 0; j < CubeFace.DIRECTIONS.length; j++) {
                CubeFace currentFace = this.getPosition();
                Vector3f currDir = CubeFace.DIRECTIONS[j];
                for (int i = 0; i < range; i++) {
                    Movement movement = manager.getMap().getNextCubeFaceInWalkDirection(currentFace,currDir);
                    if(movement != null){
                        CubeFace neuFace = movement.getPosition();
                        currDir = movement.getDirection();
                        Unit other = manager.getUnitAt(neuFace);
                        if(other != null){
                            if(other.getTeam() != getTeam()){
                                finalFaces.add(neuFace);
                            }
                            break;
                        }else if(!finalFaces.contains(neuFace)){
                            finalFaces.add(neuFace);
                        }
                        currentFace = neuFace;
                    }else{
                        break;
                    }
                }
                for (int twice = 0; twice < 2; twice++){
                    currentFace = this.getPosition();
                    currDir = CubeFace.DIRECTIONS[j];
                    boolean left = twice%2 == 0;
                    for (int i = 0; i < range*2; i++) {
                        Movement movement = manager.getMap().getNextCubeFaceInWalkDirection(currentFace,currDir);
                        if(movement != null){
                            CubeFace neuFace = movement.getPosition();
                            currDir = movement.getDirection();
                            if(i % 2 == 1){
                                Unit other = manager.getUnitAt(neuFace);
                                if(other != null){
                                    if(other.getTeam() != getTeam()){
                                        finalFaces.add(neuFace);
                                    }
                                    break;
                                }else if(!finalFaces.contains(neuFace)){
                                    finalFaces.add(neuFace);
                                }
                            }
                            currentFace = neuFace;
                            currDir = CubeFace.turnAround(currentFace.getFaceDirection(),currDir,left);
                            left = ! left;
                        }else{
                            break;
                        }
                    }
                }
            }
            for (Iterator<CubeFace> it = finalFaces.iterator(); it.hasNext();) {
                CubeFace cubeFace = it.next();
                Unit other = manager.getUnitAt(cubeFace);
                if(other != null && other.getTeam() == getTeam() ){
                    it.remove();
                }
            }
            return finalFaces;
        }
    }
    private static class JumpingMover extends Unit{
        //Attribute
        protected int ranges[];
        protected int directionChanges;
        //Konstruktor
        public JumpingMover(Piece piece, CubeFace position, Vector3f direction, Team team) {
            super(piece, position, direction, team);
        }
        
        @Override
        public Unit copy() {
            JumpingMover unit = new JumpingMover(getPiece(), getPosition(), getDirection(), getTeam());
            unit.ranges = ranges;
            unit.directionChanges = directionChanges;
            return unit;
        }
        
        @Override
        public Collection<CubeFace> getValidMoveLocations(Manager manager) {
            Collection<CubeFace> finalFaces = new HashSet<>();
            for (int j = 0; j < CubeFace.DIRECTIONS.length; j++) {
                for (int twice = 0; twice < 2; twice++){
                    CubeFace currentFace = this.getPosition();
                    Vector3f currDir = CubeFace.DIRECTIONS[j];
                    boolean left = twice%2 == 0;
                    List<Integer> usedIndizes = new ArrayList<>(directionChanges);
                    finalFaces.addAll(coolRekursiveMethod(manager, new Movement(currentFace,currDir),directionChanges,left,usedIndizes));
                }
            }
            for (Iterator<CubeFace> it = finalFaces.iterator(); it.hasNext();) {
                CubeFace cubeFace = it.next();
                Unit other = manager.getUnitAt(cubeFace);
                if(other != null && other.getTeam() == getTeam() ){
                    it.remove();
                }
            }
            return finalFaces;
        }
        private Collection<CubeFace> coolRekursiveMethod(Manager manager, Movement mov, int dirChangesLeft, boolean left, List<Integer> usedRangeIndizes){
            Set<CubeFace> list = new HashSet<>(4);
            for (int i = 0; i < ranges.length; i++) {
                int dirChanges = dirChangesLeft;
                if(!usedRangeIndizes.contains(i)){
                    Movement movement = goIntoDirection(manager,mov.getPosition(), mov.getDirection(), ranges[i]);
                    if(movement == null){
                        continue;
                    }
                    dirChanges--;
                    if(dirChanges < 0){
                        list.add(movement.getPosition());
                    }else{
                        movement.setDirection(CubeFace.turnAround(movement.getPosition().getFaceDirection(), movement.getDirection(), left));
                        left = !left;
                        usedRangeIndizes.add(i);
                        list.addAll(coolRekursiveMethod(manager,movement,dirChanges,left,usedRangeIndizes));
                        usedRangeIndizes.remove(new Integer(i));
                    }
                }
            }
            return list;
        }
        private Movement goIntoDirection(Manager manager, CubeFace facePos, Vector3f dir, int dist){
            Movement mov = new Movement(facePos,dir);
            for (int i = 0; i < dist; i++) {
                mov = manager.getMap().getNextCubeFaceInWalkDirection(mov);
                if(mov == null){
                    break;
                }
            }
            return mov;
        }
    }
    private static class PawnUnit extends Unit{
        //Attribute
        protected Vector3f walkDirection;
        protected boolean firstMove;
        //Konstruktor
        public PawnUnit(Piece piece, CubeFace position, Vector3f direction, Team team) {
            super(piece, position, direction, team);
            firstMove = true;
        }
        @Override
        public Unit copy() {
            PawnUnit unit = new PawnUnit(getPiece(), getPosition(), getDirection(), getTeam());
            unit.walkDirection = walkDirection;
            return unit;
        }
        @Override
        public Collection<CubeFace> getValidMoveLocations(Manager manager) {
            Collection<CubeFace> finalFaces = new ArrayDeque<>();
            for (int twice = 0; twice < 2; twice++){
                CubeFace currentFace = this.getPosition();
                Vector3f currDir = walkDirection;
                boolean left = twice%2 == 0;
                for (int i = 0; i < 2; i++) {
                    Movement movement = manager.getMap().getNextCubeFaceInWalkDirection(currentFace,currDir);
                    if(movement != null){
                        CubeFace neuFace = movement.getPosition();
                        currDir = movement.getDirection();
                        if(i % 2 == 1){
                            Unit other = manager.getUnitAt(neuFace);
                            if(other != null){
                                if(other.getTeam() != getTeam()){
                                    finalFaces.add(neuFace);
                                }
                                break;
                            }
                        }
                        currentFace = neuFace;
                        currDir = CubeFace.turnAround(currentFace.getFaceDirection(),currDir,left);
                        left = ! left;
                    }else{
                        break;
                    }
                }
            }
            int count = firstMove ? 2 : 1;
            CubeFace pos = this.getPosition();
            for (int i = 0; i < count; i++) {
                Movement mov = manager.getMap().getNextCubeFaceInWalkDirection(pos, walkDirection);
                if(mov != null){
                    pos = mov.getPosition();
                    Unit other = manager.getUnitAt(pos);
                    if(other == null){
                        finalFaces.add(pos);//When walking over cliffs, the walkdirection should somehow change.
                    }else{
                        break;
                    }
                }
            }
            for (Iterator<CubeFace> it = finalFaces.iterator(); it.hasNext();) {
                CubeFace cubeFace = it.next();
                Unit other = manager.getUnitAt(cubeFace);
                if(other != null && other.getTeam() == getTeam() ){
                    it.remove();
                }
            }
            return finalFaces;
        }

        @Override
        public void move(Manager manager, CubeFace destination) {
            super.move(manager, destination);
            firstMove = false;
        }
    }
    private static class ArimaaUnit extends Unit{
        //Attribute
        protected int strength;
        //Konstruktor
        public ArimaaUnit(Piece piece, CubeFace position, Vector3f direction, Team team) {
            super(piece, position, direction, team);
        }
        @Override
        public Unit copy() {
            ArimaaUnit unit = new ArimaaUnit(getPiece(), getPosition(), getDirection(), getTeam());
            unit.strength = strength;
            return unit;
        }
        //Methoden
        @Override
        public Collection<CubeFace> getValidMoveLocations(Manager manager) {
            Collection<CubeFace> finalFaces = new ArrayDeque<>();
            Collection<CubeFace> faces = manager.getMap().getAdjacentCubeFaces(this.getPosition());
            for (CubeFace neuFace : faces) {
                Unit other = manager.getUnitAt(neuFace);
                if(other == null || other.getTeam() != getTeam()){
                    finalFaces.add(neuFace);
                }
            }
            return finalFaces;
        }
    }
    private static class NormalUnit extends Unit{
        //Attribute
        protected int range;
        //Konstruktor
        public NormalUnit(Piece piece, CubeFace position, Vector3f direction, Team team) {
            super(piece, position, direction, team);
        }
        @Override
        public Unit copy() {
            NormalUnit unit = new NormalUnit(getPiece(), getPosition(), getDirection(), getTeam());
            unit.range = range;
            return unit;
        }
        //Methoden
        @Override
        public Collection<CubeFace> getValidMoveLocations(Manager manager) {
            Collection<CubeFace> finalFaces = new ArrayDeque<>();
            Collection<CubeFace> currFaces = new ArrayDeque<>();
            currFaces.add(this.getPosition());
            for (int i = 0; i < range; i++) {
                if(!currFaces.isEmpty()){
                    Collection<CubeFace> nextFaces = new ArrayDeque<>();
                    for (Iterator<CubeFace> it = currFaces.iterator(); it.hasNext();) {
                        CubeFace cubeFace = it.next();
                        Collection<CubeFace> faces = manager.getMap().getAdjacentCubeFaces(cubeFace);
                        for (CubeFace neuFace : faces) {
                            Unit other = manager.getUnitAt(neuFace);
                            if(other != null){
                                if(other.getTeam() != getTeam()){
                                    finalFaces.add(neuFace);
                                }
                            }else if(!finalFaces.contains(neuFace)){
                                nextFaces.add(neuFace);
                            }
                        }
                    }
                    finalFaces.addAll(nextFaces);
                    currFaces = nextFaces;
                }
            }
            for (Iterator<CubeFace> it = finalFaces.iterator(); it.hasNext();) {
                CubeFace cubeFace = it.next();
                Unit other = manager.getUnitAt(cubeFace);
                if(other != null && other.getTeam() == getTeam() ){
                    it.remove();
                }
            }
            return finalFaces;
        }
    }
}
