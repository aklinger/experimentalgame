package experiment.code.logic;

import experiment.code.ai.AIMove;
import experiment.code.ai.AIThread;
import experiment.code.logic.unit.Unit;
import experiment.code.camera.QuaternionCamera;
import experiment.code.map.CoolMap;
import experiment.code.map.CubeFace;
import experiment.code.math.Ray;
import experiment.code.util.ColorUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

/**
 * Manages all the game relates stuff for one scene.
 *
 * @author Toni
 */
public class Manager {
    
    //Attribute
    private List<Unit> units;
    private CoolMap map;
    private List<Team> teams;
    private Unit selected;
    private int currentTeam;
    private int movesPerTurn;
    private int movesMade;
    private QuaternionCamera camera;
    private List<AIThread> ais;
    private AIThread aiThinking;
    //
    private static final float mouseMoveSensitivity = 0.05f;
    private static final float mouseRotateSensitivity = 0.01f;
    private static final float zoomSpeed = 1.2f;
    
    //Konstruktor
    public Manager(CoolMap map) {
        this.map = map;
        this.units = new ArrayList<>();
        this.teams = new ArrayList<>();
        this.ais = new ArrayList<>();
    }
    
    //Methoden
    public Manager copy() {
        Manager man = new Manager(map);
        for (Unit unit : units) {
            man.addUnit(unit);
        }
        man.currentTeam = currentTeam;
        man.teams = teams;
        man.movesPerTurn = movesPerTurn;
        man.movesMade = movesMade;
        return man;
    }

    public void act() {
        //distance in mouse movement from the last getDX() call.
        int dx = Mouse.getDX();
        //distance in mouse movement from the last getDY() call.
        int dy = -Mouse.getDY();
        if (Mouse.isButtonDown(2) || Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
            camera.addTranslation(new Vector3f(dx * mouseMoveSensitivity, -dy * mouseMoveSensitivity, 0));
        }
        if (Mouse.isButtonDown(1)) {
            camera.rotateQuaternionCamera(QuaternionCamera.X_AXIS, dy * mouseRotateSensitivity);
            camera.rotateQuaternionCamera(QuaternionCamera.Y_AXIS, dx * mouseRotateSensitivity);
            camera.updateQuaternionCamera();
        }
        int wheel = Mouse.getDWheel();
        if (wheel != 0) {
            wheel /= 120;
            camera.addZoom(zoomSpeed * wheel);
            //cam.addTranslation(new Vector3f(0, 0, wheel * zoomSpeed));
        }
        if (Mouse.isButtonDown(0) && aiThinking == null) {
            Ray r = Ray.getPickRay();
            CubeFace selectedFace = map.getFacePickedByRay(r);
            boolean checkForNewUnit = true;
            if (selected != null && teams.indexOf(selected.getTeam()) == currentTeam) {
                Collection<CubeFace> locations = selected.getValidMoveLocations(this);
                if (locations.contains(selectedFace)) {
                    makeMove(selected, selectedFace);
                    selected = null;
                    checkForNewUnit = false;
                }
            }
            if (checkForNewUnit) {
                Unit possibleSelected = getUnitAt(selectedFace);
                selected = possibleSelected;
            }
        }
        if (aiThinking != null && aiThinking.isResultReady()) {
            AIMove move = aiThinking.getNextStepOfDeviousMasterPlan();
            if (move == null) {
                System.out.println("YOU HAVE WON BECAUSE THE AI CAN'T MAKE ANY VALID MOVES ANYMORE.");
            } else if (!move.isValidMove(this)) {
                System.out.println("BAD AI, GO HOME, AND LEARN HOW TO COMPUTE VALID MOVES!");
            } else {
                aiThinking = null;
                makeMove(move.getUnit(), move.getFace());
            }
        }
    }

    public void render() {
        camera.lookThrough();
        map.render();
        for (Unit unit : units) {
            if (unit == selected) {
                float temp = unit.getPiece().getAvgScale();
                unit.getPiece().setScale(temp * 1.2f);
                unit.render(map);
                unit.getPiece().setScale(temp);
                Collection<CubeFace> validMoveLocations = unit.getValidMoveLocations(this);
                for (CubeFace cubeFace : validMoveLocations) {
                    ColorUtil.applyColor(unit.getTeam().getColor());
                    if (selected.getTeam() != getCurrentTeam()) {
                        map.renderFaceRing(cubeFace, 0.7f, 0.9f);
                    } else {
                        map.renderFace(cubeFace);
                    }
                }
            } else {
                unit.render(map);
            }
        }
    }

    public Unit getUnitAt(CubeFace position) {
        for (Unit unit : units) {
            if (unit.getPosition().equals(position)) {
                return unit;
            }
        }
        return null;
    }

    public void addUnit(Unit unit) {
        units.add(unit);
    }
    
    /**
     * Returns the Unit destroyed by this move.
     */
    public Unit makeMove(Unit unit, CubeFace position) {
        Unit destroyed = moveUnit(unit, position);
        movesMade++;
        if (movesMade >= movesPerTurn) {
            movesMade = 0;
            nextTurn();
        }
        for (AIThread ai : ais) {
            if (ai.getTeam() == getCurrentTeam()) {
                ai.startThinking();
                aiThinking = ai;
            }
        }
        return destroyed;
    }

    public void nextTurn() {
        currentTeam++;
        if (currentTeam >= teams.size()) {
            currentTeam = 0;
        }
    }

    /**
     * Returns the Unit destroyed by this move.
     */
    public Unit moveUnit(Unit unit, CubeFace position) {
        unit.move(this, position);
        Unit enemy = getUnitAt(position);
        if (enemy != null) {
            units.remove(enemy);
        }
        unit.setPosition(position);
        unit.getPiece().addRotation(90);
        return enemy;
    }

    //Getter & Setter
    public Team getCurrentTeam() {
        return teams.get(currentTeam);
    }

    public int getCurrentTeamIndex() {
        return currentTeam;
    }

    public CoolMap getMap() {
        return map;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public QuaternionCamera getCamera() {
        return camera;
    }

    public void setCamera(QuaternionCamera camera) {
        this.camera = camera;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public List<AIThread> getAis() {
        return ais;
    }

    public void setMovesPerTurn(int movesPerTurn) {
        this.movesPerTurn = movesPerTurn;
    }
}
