package experiment.code.ai;

import experiment.code.logic.Manager;
import experiment.code.logic.Team;
import experiment.code.logic.unit.Unit;
import experiment.code.map.CubeFace;
import java.util.Collection;

/**
 * The class that handles the AI.
 * 
 * @author Toni
 */
public class AIThread implements Runnable{
    //Attribute
    private Thread thread;
    private long maxThinkMillis;
    private long startMillis;
    private int waitTime;
    private boolean ready;
    private Manager manager;
    private Team team;
    private AIValueSystem weight;
    private AIMove nextStep;
    private int maxDepth;
    //Konstruktor
    public AIThread(Manager manager, Team team, int maxDepth, int maxThinkMillis) {
        this.manager = manager;
        this.maxThinkMillis = maxThinkMillis;
        this.team = team;
        this.maxDepth = maxDepth;
        
        weight = new SimpleAISystem();
        
        ready = false;
        
        waitTime = 10;
    }
    
    //Methoden
    @Override
    public void run(){
        nextStep = getBestNextMove(manager);
        ready = true;
    }
    private AIMove getBestNextMove(Manager manState){
        Team thaTeam = manState.getTeams().get(manState.getCurrentTeamIndex());
        float best = Float.MIN_VALUE;
        AIMove move = null;
        for (int i = 0; i < manState.getUnits().size();i++) {
            Unit originalUnit = manState.getUnits().get(i);
            if(originalUnit.getTeam() == thaTeam){
                Collection<CubeFace> validMoveLocations = originalUnit.getValidMoveLocations(manState);
                for(CubeFace location : validMoveLocations){
                    AIMove thatMove = new AIMove(originalUnit,location);
                    Manager man = manState.copy();
                    float nextBestValue = getBestValue(man,thatMove,1);
                    if(nextBestValue > best){
                        best = nextBestValue;
                        move = thatMove;
                    }
                }
            }
        }
        return move;
    }
    private float getBestValue(Manager manState, AIMove move, int depth){
        Unit theOminousUnit = move.getUnit();
        Unit theImpostor = theOminousUnit.copy();
        manState.getUnits().remove(theOminousUnit);
        Manager man = manState.copy();
        man.getUnits().add(theImpostor);
        Unit destroyed = man.makeMove(theImpostor, move.getFace());
        float value = (float)Math.random();
        if(destroyed != null){
            if(destroyed.getTeam() != team){
                value = weight.getValueOfDestroyingUnit(destroyed, man);
            }else{
                value = weight.getValueOfLosingUnit(destroyed, man);
            }
        }
        /*try {
            Thread.sleep(waitTime);
        } catch (InterruptedException ex) {
            Logger.getLogger(AIThread.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        if(!isThinkTimeOver() && depth < maxDepth){
            //System.out.println("DEPTH: "+depth);
            float best = Integer.MIN_VALUE;
            Team thaTeam = man.getCurrentTeam();
            for (int i = 0; i < man.getUnits().size(); i++) {
                Unit unit = man.getUnits().get(i);
                if(unit.getTeam() == thaTeam){
                    Collection<CubeFace> validMoveLocations = unit.getValidMoveLocations(man);
                    for(CubeFace location : validMoveLocations){
                        float nextBestValue = getBestValue(man,new AIMove(unit,location),depth+1);
                        nextBestValue = (float)((nextBestValue+1)*Math.random());
                        if(nextBestValue > best){
                            best = nextBestValue;
                        }
                    }
                }
            }
            if(best != Integer.MIN_VALUE){
                value += best;
            }
        }/*else{
            System.out.println("(!isThinkTimeOver()):"+(!isThinkTimeOver()));
            System.out.println("depth < maxDepth:"+(depth < maxDepth));
        }*/
        manState.getUnits().add(theOminousUnit);
        return value;
    }
    private boolean isThinkTimeOver(){
        return System.currentTimeMillis()-startMillis>maxThinkMillis;
    }
    public void startThinking(){
        ready = false;
        startMillis = System.currentTimeMillis();
        thread = new Thread(this);
        thread.start();
    }
    public boolean isResultReady(){
        return ready;
    }
    public AIMove getNextStepOfDeviousMasterPlan(){
        return nextStep;
    }

    public Team getTeam() {
        return team;
    }

    public void setWeight(AIValueSystem weight) {
        this.weight = weight;
    }
}
