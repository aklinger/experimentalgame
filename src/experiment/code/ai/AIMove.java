package experiment.code.ai;

import experiment.code.logic.Manager;
import experiment.code.logic.unit.Unit;
import experiment.code.map.CubeFace;

/**
 * Defines which unit the AI wants to move where.
 * 
 * @author Toni
 */
public class AIMove {
    //Attribute
    private Unit unit;
    private CubeFace face;
    //Konstruktor
    public AIMove(Unit unit, CubeFace face) {
        this.unit = unit;
        this.face = face;
    }
    //Methoden
    public boolean isValidMove(Manager manager){
        return (unit.getValidMoveLocations(manager).contains(face));
    }
    //Getter & Setter
    public CubeFace getFace() {
        return face;
    }

    public Unit getUnit() {
        return unit;
    }
    
    public void setFace(CubeFace face) {
        this.face = face;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}
