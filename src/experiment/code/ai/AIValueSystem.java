package experiment.code.ai;

import experiment.code.logic.Manager;
import experiment.code.logic.unit.Unit;

/**
 * Determines how valueable an AI sees specific units.
 * 
 * @author Toni
 */
public abstract class AIValueSystem {
    //Methoden
    public abstract int getValueOfLosingUnit(Unit unit, Manager man);
    public abstract int getValueOfDestroyingUnit(Unit unit, Manager man);
}
