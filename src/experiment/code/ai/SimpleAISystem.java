package experiment.code.ai;

import experiment.code.logic.Manager;
import experiment.code.logic.UnitFactory;
import experiment.code.logic.unit.Unit;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple AIValueSystem.
 * 
 * @author Toni
 */
public class SimpleAISystem extends AIValueSystem{
    //Attribute
    private Map<Integer,Integer> values;

    //Konstruktor
    public SimpleAISystem() {
        values = new HashMap<>();
        values.put(UnitFactory.KING, 100);
        values.put(UnitFactory.QUEEN, 12);
        values.put(UnitFactory.KNIGHT, 5);
        values.put(UnitFactory.BISHOP, 8);
        values.put(UnitFactory.TOWER, 9);
        values.put(UnitFactory.PAWN, 1);
        values.put(UnitFactory.JUMPER, 7);
        values.put(UnitFactory.PEASANT, 3);
        values.put(UnitFactory.SWEEPER, 6);
        values.put(UnitFactory.DRILL, 8);
    }
    //Methoden
    @Override
    public int getValueOfLosingUnit(Unit unit, Manager man) {
        if(unit == null || values == null){
            System.out.println("Shouldn't happen");
            return 0;
        }
        try{
            return values.get(unit.getType()*2);
        }catch (NullPointerException npe){
            System.out.println("Should happen even less likely...!");
            return 0;
        }
    }
    @Override
    public int getValueOfDestroyingUnit(Unit unit, Manager man) {
        if(unit == null || values == null){
            System.out.println("Shouldn't happen");
            return 0;
        }
        try{
            return values.get(unit.getType());
        }catch (NullPointerException npe){
            System.out.println("Should happen even less likely...!");
            return 0;
        }
    }
}
