package experiment.code.math;

import org.lwjgl.util.vector.Vector3f;

/**
 * A class to represent a Vector in a 3D space.
 * <p/>
 * Notify this class is immutable (e.g. like String class).
 * <p/>
 * @author Toni
 */
public class Vector3D {
    //Attribute
    private double x;
    private double y;
    private double z;
    //Static Attributes
    public static final Vector3D ZERO = new Vector3D(0);
    public static final Vector3D ONE = new Vector3D(1);

    //Konstruktor
    public Vector3D(double value) {
        this(value, value, value);
    }

    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public static Vector3D fromVector3f(Vector3f floatVector){
        return new Vector3D(floatVector.x,floatVector.y,floatVector.z);
    }
    //Methoden
    public Vector3D add(Vector3D other) {
        return new Vector3D(x + other.x, y + other.y, z + other.z);
    }

    public Vector3D sub(Vector3D other) {
        return new Vector3D(x - other.x, y - other.y, z - other.z);
    }

    public Vector3D mul(Vector3D other) {
        return new Vector3D(x * other.x, y * other.y, z * other.z);
    }

    public Vector3D div(Vector3D other) {
        return new Vector3D(x / other.x, y / other.y, z / other.z);
    }

    public double getLength() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    public double getSquareLength() {
        return x * x + y * y + z * z;
    }

    public Vector3D getVectorTo(Vector3D from, Vector3D to) {
        return new Vector3D(to.x - from.x, to.y - from.y, to.z - from.z);
    }

    public Vector3f toVector3f(){
        return new Vector3f((float)x, (float)y, (float)z);
    }
    
    //Modifiers
    public Vector3D setX(double x) {
        return new Vector3D(x, y, z);
    }

    public Vector3D setY(double y) {
        return new Vector3D(x, y, z);
    }

    public Vector3D setZ(double z) {
        return new Vector3D(x, y, z);
    }

    //Getter & Setter
    /**
     * Gets the x value of this vector.
     * @return The x value.
     */
    public double x() {
        return x;
    }

    /**
     * Gets the y value of this vector.
     * @return The y value.
     */
    public double y() {
        return y;
    }

    /**
     * Gets the z value of this vector.
     * @return The z value.
     */
    public double z() {
        return z;
    }
}
