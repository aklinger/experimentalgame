package experiment.code.math;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Toni
 */
public class Ray {
    //Attribute
    private Vector3f position;
    private Vector3f direction;
    
    //Konstruktor
    public Ray(Vector3f position, Vector3f direction) {
        this.position = position;
        this.direction = direction;
    }
    //Methoden
    /**
     * Calculates the Ray that extends from the mouse-position into the virtual world depending on the Modelview and Projection Matrix.
     * @return The ray at the Mouse Position in world coordinates useful for 3D-Picking.
     */
    public static Ray getPickRay(){
        float winx = Mouse.getX();
        float winy = Mouse.getY();
        FloatBuffer modelMatrix = BufferUtils.createFloatBuffer(16);
        FloatBuffer projMatrix = BufferUtils.createFloatBuffer(16);
        IntBuffer viewport = BufferUtils.createIntBuffer(16);
        GL11.glGetFloat(GL11.GL_MODELVIEW_MATRIX, modelMatrix);
        GL11.glGetFloat(GL11.GL_PROJECTION_MATRIX, projMatrix);
        GL11.glGetInteger(GL11.GL_VIEWPORT, viewport);
        FloatBuffer obj1 = BufferUtils.createFloatBuffer(3);
        FloatBuffer obj2 = BufferUtils.createFloatBuffer(3);
        
        boolean result1 = GLU.gluUnProject(winx, winy, 0, modelMatrix, projMatrix, viewport, obj1);
        boolean result2 = GLU.gluUnProject(winx, winy, 1, modelMatrix, projMatrix, viewport, obj2);
        if(!result1 || !result2){
            throw new RuntimeException("Something went wrong while Calculating the view Vector.");
        }
        Vector3f point1 = new Vector3f(obj1.get(0), obj1.get(1), obj1.get(2));
        Vector3f point2 = new Vector3f(obj2.get(0), obj2.get(1), obj2.get(2));
        
        return new Ray(point1, Vector3f.sub(point2, point1, point2));
    }
    /**
     * Returns the distance from the origin of the Ray to the intersection point from the Ray and the triangle.
     * 
     * If no intersection occurs, Float.MAX_VALUE is returned.
     * 
     * @param R The ray to check.
     * @param vertex1 The first point of the triangle
     * @param vertex2 The second point of the triangle
     * @param vertex3 The third point of the triangle
     * @return The distance to the intersection point.
     */
    public static float rayIntersectsTriangle(Ray R, Vector3f vertex1, Vector3f vertex2, Vector3f vertex3) {
        // Compute vectors along two edges of the triangle.
        Vector3f edge1 = null, edge2 = null;

        edge1 = Vector3f.sub(vertex2, vertex1, edge1);
        edge2 = Vector3f.sub(vertex3, vertex1, edge2);

        // Compute the determinant.
        Vector3f directionCrossEdge2 = null;
        directionCrossEdge2 = Vector3f.cross(R.getDirection(), edge2, directionCrossEdge2);


        float determinant = Vector3f.dot(directionCrossEdge2, edge1);
        // If the ray and triangle are parallel, there is no collision.
        if (determinant > -.0000001f && determinant < .0000001f) {
            return Float.MAX_VALUE;
        }

        float inverseDeterminant = 1.0f / determinant;

        // Calculate the U parameter of the intersection point.
        Vector3f distanceVector = null;
        distanceVector = Vector3f.sub(R.getPosition(), vertex1, distanceVector);


        float triangleU = Vector3f.dot(directionCrossEdge2, distanceVector);
        triangleU *= inverseDeterminant;

        // Make sure the U is inside the triangle.
        if (triangleU < 0 || triangleU > 1) {
            return Float.MAX_VALUE;
        }

        // Calculate the V parameter of the intersection point.
        Vector3f distanceCrossEdge1 = null;
        distanceCrossEdge1 = Vector3f.cross(distanceVector, edge1, distanceCrossEdge1);


        float triangleV = Vector3f.dot(R.getDirection(), distanceCrossEdge1);
        triangleV *= inverseDeterminant;

        // Make sure the V is inside the triangle.
        if (triangleV < 0 || triangleU + triangleV > 1) {
            return Float.MAX_VALUE;
        }

        // Get the distance to the face from our ray origin
        float rayDistance = Vector3f.dot(distanceCrossEdge1, edge2);
        rayDistance *= inverseDeterminant;


        // Is the triangle behind us?
        if (rayDistance < 0) {
            rayDistance *= -1;
            return Float.MAX_VALUE;
        }
        return rayDistance;
    }
    //Getter & Setter
    public Vector3f getDirection() {
        return direction;
    }

    public Vector3f getPosition() {
        return position;
    }
}
