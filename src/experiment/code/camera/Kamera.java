package experiment.code.camera;

import experiment.code.math.Vector3D;
import org.lwjgl.opengl.GL11;

/**
 * A camera to define the viewport for the rendering.
 * <p/>
 * @author Toni
 */
public class Kamera {
    //Attribute
    private Vector3D position;
    //the rotation around the Y axis of the camera
    private float yaw = 0.0f;
    //the rotation around the X axis of the camera
    private float pitch = 0.0f;

    //Konstruktor
    public Kamera() {
        this(Vector3D.ZERO);
    }

    public Kamera(Vector3D position) {
        this.position = position;
    }
    //Methoden
    public void lookThrough() {
        //translate to the position vector's location
        GL11.glTranslated(position.x(), position.y(), position.z());
        //roatate the pitch around the X axis
        GL11.glRotated(pitch, 1.0f, 0.0f, 0.0f);
        //roatate the yaw around the Y axis
        GL11.glRotated(yaw, 0.0f, 1.0f, 0.0f);
    }
    //Getter & Setter
    public Vector3D getPosition() {
        return position;
    }

    public void setPosition(Vector3D position) {
        this.position = position;
    }

    public void addPosition(Vector3D movement) {
        this.position = position.add(movement);
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }
}
