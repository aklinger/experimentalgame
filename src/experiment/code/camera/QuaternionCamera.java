package experiment.code.camera;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

/**
 *
 * @author Toni
 */
public class QuaternionCamera {
    //Attribute
    private FloatBuffer matrixBuffer;
    private Quaternion quatCurrentRotation;
    private Quaternion quatRotationChange;
    private Vector3f rotatoryAnchor;
    private Vector3f translation;
    private float zoom;
    private float minZoom;
    private float maxZoom;
    //
    public static final Vector3f X_AXIS = new Vector3f(1, 0, 0);
    public static final Vector3f Y_AXIS = new Vector3f(0, 1, 0);
    public static final Vector3f Z_AXIS = new Vector3f(0, 0, 1);

    //Konstruktor
    public QuaternionCamera() {
        this(new Quaternion(1, 0, 0, 0), new Vector3f(), new Vector3f());
    }

    public QuaternionCamera(Quaternion quatCurrentRotation, Vector3f rotatoryAnchor, Vector3f translation) {
        this.quatCurrentRotation = quatCurrentRotation;
        this.rotatoryAnchor = rotatoryAnchor;
        this.translation = translation;

        this.zoom = 1f;
        this.minZoom = 0f;
        this.maxZoom = Float.MAX_VALUE;
        this.matrixBuffer = BufferUtils.createFloatBuffer(16);
        this.quatRotationChange = new Quaternion();

        createMatrixBuffer();
    }

    //Methoden
    public void lookThrough() {
        //translate to the position vector's location
        GL11.glTranslated(translation.x, translation.y, translation.z);

        GL11.glMultMatrix(matrixBuffer);

        GL11.glScaled(zoom, zoom, zoom);

        GL11.glTranslated(rotatoryAnchor.x, rotatoryAnchor.y, rotatoryAnchor.z);
    }

    public final void rotateQuaternionCamera(Vector3f axis, float rotation) {
        Vector4f rotationVector = new Vector4f(axis.x, axis.y, axis.z, rotation);
        quatRotationChange.setFromAxisAngle(rotationVector);
        Quaternion.mul(quatRotationChange, quatCurrentRotation, quatCurrentRotation);
    }

    public void updateQuaternionCamera() {
        quatCurrentRotation.normalise();
        createMatrixBuffer();
    }

    /**
     * http://www.java-gaming.org/index.php?topic=24177.0
     * http://www.genesis3d.com/~kdtop/Quaternions-UsingToRepresentRotation.htm
     */
    private void createMatrixBuffer() {
        float w, x, y, z;
        w = quatCurrentRotation.w;
        x = quatCurrentRotation.x;
        y = quatCurrentRotation.y;
        z = quatCurrentRotation.z;
        Matrix4f m = new Matrix4f();

        m.m00 = sq(w) + sq(x) - sq(y) - sq(z);
        m.m01 = 2 * x * y + 2 * w * z;
        m.m02 = 2 * x * z - 2 * w * y;
        m.m03 = 0;

        m.m10 = 2 * x * y - 2 * w * z;
        m.m11 = sq(w) - sq(x) + sq(y) - sq(z);
        m.m12 = 2 * y * z + 2 * w * x;
        m.m13 = 0;

        m.m20 = 2 * x * z + 2 * w * y;
        m.m21 = 2 * y * z - 2 * w * x;
        m.m22 = sq(w) - sq(x) - sq(y) + sq(z);
        m.m23 = 0;

        m.m30 = 0;
        m.m31 = 0;
        m.m32 = 0;
        m.m33 = sq(w) + sq(x) + sq(y) + sq(z);

        matrixBuffer.clear();
        m.store(matrixBuffer);
        matrixBuffer.flip();
    }

    private float sq(float x) {
        return x * x;
    }

    public void addTranslation(Vector3f delta) {
        translation.translate(delta.x, delta.y, delta.z);
    }
    public void addZoom(float delta){
        if(delta>0){
            zoom *= delta;
        }else{
            zoom /= -delta;
        }
        checkZoom();
    }
    public void zoomIn(float delta){
        zoom *= delta;
        checkZoom();
    }
    
    public void zoomOut(float delta){
        zoom /= delta;
        checkZoom();
    }
    
    private void checkZoom(){
        if (zoom > maxZoom) {
            zoom = maxZoom;
        } else if (zoom < minZoom) {
            zoom = minZoom;
        }
    }

    //Getter & Setter
    public Quaternion getQuatCurrentRotation() {
        return quatCurrentRotation;
    }

    public void setQuatCurrentRotation(Quaternion quatCurrentRotation) {
        this.quatCurrentRotation = quatCurrentRotation;
    }

    public Vector3f getRotatoryAnchor() {
        return rotatoryAnchor;
    }

    public void setRotatoryAnchor(Vector3f rotatoryAnchor) {
        this.rotatoryAnchor = rotatoryAnchor;
    }

    public Vector3f getTranslation() {
        return translation;
    }

    public void setTranslation(Vector3f translation) {
        this.translation = translation;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }
    public void setZoomLimits(float minZoom, float maxZoom){
        this.minZoom = minZoom;
        this.maxZoom = maxZoom;
    }

    public FloatBuffer getMatrixBuffer() {
        return matrixBuffer;
    }
    
}
