package experiment.code.gl;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.GL_AMBIENT_AND_DIFFUSE;
import static org.lwjgl.opengl.GL11.GL_COLOR_MATERIAL;
import static org.lwjgl.opengl.GL11.GL_DIFFUSE;
import static org.lwjgl.opengl.GL11.GL_FRONT;
import static org.lwjgl.opengl.GL11.GL_LIGHT0;
import static org.lwjgl.opengl.GL11.GL_LIGHTING;
import static org.lwjgl.opengl.GL11.GL_LIGHT_MODEL_AMBIENT;
import static org.lwjgl.opengl.GL11.GL_POSITION;
import static org.lwjgl.opengl.GL11.GL_SHININESS;
import static org.lwjgl.opengl.GL11.GL_SPECULAR;
import static org.lwjgl.opengl.GL11.glColorMaterial;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glLight;
import static org.lwjgl.opengl.GL11.glLightModel;
import static org.lwjgl.opengl.GL11.glMaterial;
import static org.lwjgl.opengl.GL11.glMaterialf;
import static org.lwjgl.opengl.GL11.glShadeModel;

/**
 *
 * @author Toni
 */
public class Light {
    //Attribute
    private FloatBuffer matSpecular;
    private FloatBuffer lightPosition;
    private FloatBuffer diffuseLight;
    private FloatBuffer specularLight;
    private FloatBuffer lModelAmbient;
    private float shininess;

    //Konstruktor
    public Light(){
        matSpecular = BufferUtils.createFloatBuffer(4);
        matSpecular.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();

        lightPosition = BufferUtils.createFloatBuffer(4);
        lightPosition.put(1.0f).put(1.0f).put(0.0f).put(0.0f).flip();

        float stuff = 0.8f;
        diffuseLight = BufferUtils.createFloatBuffer(4);
        diffuseLight.put(stuff).put(stuff).put(stuff).put(stuff).flip();

        float stuff2 = 0.2f;
        specularLight = BufferUtils.createFloatBuffer(4);
        specularLight.put(stuff2).put(stuff2).put(stuff2).put(stuff2).flip();

        lModelAmbient = BufferUtils.createFloatBuffer(4);
        lModelAmbient.put(0.65f).put(0.65f).put(0.65f).put(1.0f).flip();
        
        shininess = 10f;
    }
    public Light(FloatBuffer matSpecular, FloatBuffer lightPosition, FloatBuffer whiteLight, FloatBuffer specularLight, FloatBuffer lModelAmbient, float shininess) {
        this.matSpecular = matSpecular;
        this.lightPosition = lightPosition;
        this.diffuseLight = whiteLight;
        this.specularLight = specularLight;
        this.lModelAmbient = lModelAmbient;
    }
    //Methoden
    public void initOpenGLLighting() {
        glEnable(GL11.GL_NORMALIZE);
        glShadeModel(GL11.GL_FLAT);//GL_SMOOTH
        glMaterial(GL_FRONT, GL_SPECULAR, matSpecular); // sets specular material color
        glMaterialf(GL_FRONT, GL_SHININESS, shininess);	// sets shininess

        glLight(GL_LIGHT0, GL_POSITION, lightPosition); // sets light position
        glLight(GL_LIGHT0, GL_SPECULAR, specularLight);	// sets specular light
        glLight(GL_LIGHT0, GL_DIFFUSE, diffuseLight);	// sets diffuse light
        glLightModel(GL_LIGHT_MODEL_AMBIENT, lModelAmbient);// global ambient light 

        glEnable(GL_LIGHTING);				// enables lighting
        glEnable(GL_LIGHT0);				// enables light0

        glEnable(GL_COLOR_MATERIAL);			// enables opengl to use glColor3f to define material color
        glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);// tell opengl glColor3f effects the ambient and diffuse properties of material
    }
}
