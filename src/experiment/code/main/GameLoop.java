package experiment.code.main;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

/**
 * This class is awesome and also the main part of the GameLoop.
 * <p/>
 * @author Toni
 */
public abstract class GameLoop implements Runnable {
    //Static
    public static final Logger LOGGER = Logger.getLogger(GameLoop.class.getName());
    //Attribute
    private int framerate = 60;
    private boolean vsync = false;
    //Closing
    private int escaping = 0;
    private int escapeTime = (int) (1.5 * framerate);
    private boolean easyEscape = true;
    //Framerate-Debugging
    private long frameNanos;
    private long[] frameRates;
    private int framePointer;
    //Running
    private boolean running;
    //Konstruktor

    //Starting
    private void initAll() throws Exception {
        initStuff();
        running = true;
    }

    //Initalisieren
    /**
     * Initialize everything here, that has to be initialized before the Main
     * Loop.
     */
    protected abstract void initStuff() throws Exception;

    //Methoden
    @Override
    public void run() {
        try {
            initAll();
            Display.setVSyncEnabled(vsync);
            //
            frameRates = new long[framerate];
            for (int i = 0; i < frameRates.length; i++) {
                frameRates[i] = 0;
            }
            long frameStart;
            //
            while ((running) && (!easyEscape || escaping <= escapeTime) && !Display.isCloseRequested()) {
                frameStart = System.nanoTime();
                //
                gameLoop();
                Display.update();
                //
                frameNanos = System.nanoTime() - frameStart;
                calcAverageFrameRate();
                //
                Display.sync(framerate);
            }
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            cleanup();
        }
    }

    private void gameLoop() {
        checkForEscapeSequence();
        act();
        render();
    }

    private void calcAverageFrameRate() {
        framePointer++;
        framePointer %= frameRates.length;
        frameRates[framePointer] = (int) (1_000_000_000d / (double) frameNanos);
    }

    //Acting
    private void checkForEscapeSequence() {
        if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
            escaping++;
        } else {
            escaping = 0;
        }
    }

    /**
     * Here goes all the logic that has to happen every frame.
     */
    public abstract void act();

    //Rendern
    /**
     * Here goes all the rendering that has to be done every frame.
     * <p/>
     * May be skipped during heavy load in some implementations.
     */
    public abstract void render();
    
    //Cleanup
    /**
     * What should happen after the Main-Loop is finished, and the game is
     * exiting.
     */
    protected abstract void cleanup();
    
    //Getter & Setter
    public long getCurrentNanoSeconds() {
        return frameNanos;
    }

    public long getCurrentFPS() {
        return frameRates[framePointer];
    }

    public long getAvgFPS() {
        long sum = 0;
        for (int i = 0; i < frameRates.length; i++) {
            sum += frameRates[i];
        }
        return (long) (sum / frameRates.length);
    }

    public void setFPS(int fps) {
        framerate = fps;
    }

    public void setEscapeTime(int escapeTime) {
        this.escapeTime = escapeTime;
    }

    public void setEasyEscape(boolean easyEscape) {
        this.easyEscape = easyEscape;
    }

    public void quit() {
        this.running = false;
    }

    public void setVsync(boolean vsync) {
        this.vsync = vsync;
    }
}
