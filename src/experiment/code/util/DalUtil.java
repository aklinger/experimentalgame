package experiment.code.util;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import javax.swing.JFileChooser;

/**
 * This is an Util-class for the Data Access Layer.
 * 
 * @author Toni
 */
public class DalUtil {
    //Attribute
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    //Konstruktor
    /**
     * Utility-Class, no instancing.
     */
    private DalUtil(){}
    //Methoden
    /**
     * Resolves a name of a file inside the Project to a path usable for the loader.
     * 
     * @param path The relative path of the file.
     * @return The absolute URL for loading the file.
     */
    public static URL asURL(String path){
        URL url = Thread.currentThread().getContextClassLoader().getResource(path);
        if(url == null){
            throw new RuntimeException("Couldn't find: "+path);
        }
        return url;
    }
    /**
     * Resolves a name of a file inside the Project to a stream usable for loading.
     * 
     * @param path The relative path of the file.
     * @return An InputStream for loading the file.
     * @throws FileNotFoundException 
     */
    public static InputStream asStream(String path) throws FileNotFoundException{
        InputStream stream = DalUtil.class.getResourceAsStream(path);
        if(stream == null){
            throw new FileNotFoundException("Couldn't find: "+path);
        }
        return stream;
    }
    /**
     * Quick and Dirty method for getting a File via a FileChooser.
     * @return the selected File or null if no File was selected.
     */
    public static File showOpenDialog(){
        JFileChooser fc = new JFileChooser();
        if(fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
            return fc.getSelectedFile();
        }
        return null;
    }
    /**
     * Quick and Dirty method for getting a File via a FileChooser.
     * @return the selected File or null if no File was selected.
     */
    public static File showSaveDialog(){
        JFileChooser fc = new JFileChooser();
        if(fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
            return fc.getSelectedFile();
        }
        return null;
    }
    /**
     * Returns the Location of jar file being currently executed.
     * @param jarname If the name of the jar itself should be included or not
     * @return The Path of the current jar file.
     * @throws UnsupportedEncodingException 
     */
    public static String getPathOfThisJar(boolean jarname) throws UnsupportedEncodingException{
        String path = removeFirst(DalUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath(),"/");
        String decodedPath = URLDecoder.decode(path, "UTF-8");
        if(!jarname){
            decodedPath = removeLast(decodedPath,"/");
        }
        //Log.log(Level.CONFIG,"Path of this jar: {0}",decodedPath);
        return decodedPath;
    }
    /**
     * Copies a file to a new Destination.
     * @param in The InputStream of the original file.
     * @param out The OutputStream to where it should be copied.
     * @throws IOException 
     */
    public static void copy(InputStream in, OutputStream out) throws IOException{
        int data;
        while((data = in.read()) != -1)
        {
            out.write(data);
        }
        out.flush();
        in.close();
        out.close();
    }
    /**
     * A String-Util method.
     * Removes everything to the first occurrence of the pattern in the String.
     */
    private static String removeFirst(String text, String pattern){
        return text.substring(text.indexOf(pattern)+1);
    }
    /**
     * A String-Util method.
     * Removes everything from the last occurrence of the pattern in the String.
     */
    private static String removeLast(String text, String pattern){
        int index = text.lastIndexOf(pattern);
        if(index != -1){
            return text.substring(0,index);
        }
        return text;
    }
}
