package experiment.code.util;

/**
 * This class is used to be able to treat java.awt.DisplayMode 
 * and org.lwjgl.opengl.DisplayMode in the same way.
 * Only saves width, height and color bit depth.
 * 
 * @author Toni
 */
public class GenericDisplayMode implements Comparable<GenericDisplayMode>{
    //Attribute
    private int width;
    private int height;
    private int bitDepth;
    private org.lwjgl.opengl.DisplayMode lwjglMode;
    //Konstruktor
    /**
     * Creates a new GenericDisplayMode with the specified attributes.
     * @param width The width in pixels.
     * @param height The height in pixels.
     * @param bitDepth The bitDepth of the colors.
     */
    public GenericDisplayMode(int width, int height, int bitDepth) {
        this.width = width;
        this.height = height;
        this.bitDepth = bitDepth;
    }
    /**
     * Creates a new GenericDisplayMode out of a java.awt.DisplayMode object.
     * @param awtMode The DisplayMode
     */
    public GenericDisplayMode(java.awt.DisplayMode awtMode){
        this(awtMode.getWidth(), awtMode.getHeight(), awtMode.getBitDepth());
    }
    /**
     * Creates a new GenericDisplayMode out of a org.lwjgl.opengl.DisplayMode object.
     * @param lwjglMode The DisplayMode
     */
    public GenericDisplayMode(org.lwjgl.opengl.DisplayMode lwjglMode){
        this(lwjglMode.getWidth(), lwjglMode.getHeight(), lwjglMode.getBitsPerPixel());
        this.lwjglMode = lwjglMode;
    }
    //Comparable
    @Override
    public int compareTo(GenericDisplayMode other) {
        if(this.width == other.width){
            if(this.height == other.height){
                if(this.bitDepth == other.bitDepth){
                    if(this.lwjglMode != null && other.lwjglMode != null){
                        return this.lwjglMode.getFrequency()-other.lwjglMode.getFrequency();
                    }else{
                        return 0;
                    }
                }else{
                    return this.bitDepth-other.bitDepth;
                }
            }else{
                return this.height-other.height;
            }
        }else{
            return this.width-other.width;
        }
    }
    //Overrides - Object
    @Override
    public int hashCode(){
        return width ^ height ^ bitDepth;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GenericDisplayMode other = (GenericDisplayMode) obj;
        if (this.width != other.width) {
            return false;
        }
        if (this.height != other.height) {
            return false;
        }
        if (this.bitDepth != other.bitDepth) {
            return false;
        }
        return true;
    }
    @Override
    public String toString() {
        return width+"x"+height+"_"+bitDepth+"bit";
    }
    //Getter & Setter
    public java.awt.DisplayMode getAwtDisplayMode(){
        return new java.awt.DisplayMode(width, height, bitDepth, java.awt.DisplayMode.REFRESH_RATE_UNKNOWN);
    }
    public org.lwjgl.opengl.DisplayMode getLwjglDisplayMode(){
        return lwjglMode != null ? lwjglMode : new org.lwjgl.opengl.DisplayMode(width, height);
    }
    public int getBitDepth() {
        return bitDepth;
    }
    public int getHeight() {
        return height;
    }
    public int getWidth() {
        return width;
    }
}
