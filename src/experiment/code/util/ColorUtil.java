package experiment.code.util;

import java.awt.Color;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Toni
 */
public class ColorUtil {
    //Methoden
    /**
     * Sets the current Color of the OpenGL instance.
     * <p/>
     * @param color The new Color.
     */
    public static void applyColor(Color color) {
        float[] colors = color.getComponents(null);
        GL11.glColor4f(colors[0], colors[1], colors[2], colors[3]);
    }
    /**
     * Creates a random Color.
     */
    public static Color randomColor(){
        return new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255));
    }
}
