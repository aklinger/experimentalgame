package experiment.code.util;

import java.awt.Color;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector2f;

/**
 * This class provides a lot of useful methods, for easier use of OpenGL and
 * LWJGL.
 * <p/>
 * @author Toni
 */
public class DisplayUtil {
    //Attribute
    //Konstruktor
    /**
     * Utility class, no instancing.
     */
    private DisplayUtil() {
    }
    //Methoden
    //Initialisierungen
    /**
     * Creates a Window with LWJGL with the specified settings. The Display also
     * is the OpenGL rendering-context.
     * <p/>
     * @param mode The LWJGL-DisplayMode to use.
     * @param title The Title-Text of the Window.
     * @param fullscreen If the window should be fullscreen.
     * @throws LWJGLException
     */
    public static void initWindow(org.lwjgl.opengl.DisplayMode mode, String title, boolean fullscreen) throws LWJGLException {
        Display.setDisplayMode(mode);
        Display.setTitle(title);
        Display.setFullscreen(fullscreen);
        Display.create();
    }
    /**
     * Destroys the Display.
     */
    public static void cleanup() {
        Display.destroy();
    }
    /**
     * Sets the Settings of OpenGL like specified.
     * <p/>
     * @param clearColor The Color used for clearing.
     * @param alpha If alpha is enabled.
     * @param textures If textures are enabled.
     * @param depthTest If the depthTest is enabled, only necessary for 3D.
     * @param lighting If lighting is enabled.
     */
    public static void initOpenGL(Color clearColor, boolean alpha, boolean textures, boolean depthTest, boolean lighting) {
        //Setting the render settings
        if (alpha) {
            //Really basic and most common alpha blend function
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        } else {
            GL11.glDisable(GL11.GL_BLEND);
        }
        if (textures) {
            GL11.glEnable(GL11.GL_TEXTURE_2D);
        } else {
            GL11.glDisable(GL11.GL_TEXTURE_2D);
        }
        if (depthTest) {
            GL11.glEnable(GL11.GL_DEPTH_TEST);
        } else {
            GL11.glDisable(GL11.GL_DEPTH_TEST);
        }
        if (lighting) {
            GL11.glEnable(GL11.GL_LIGHTING);
        } else {
            GL11.glDisable(GL11.GL_LIGHTING);
        }
        //
        setClearColor(clearColor);
    }
    /**
     * Initializes a standard Orthographic View and sets the left-upper corner
     * as the coordinate root.
     */
    public static void initOrthoView(double zNear, double zFar) {
        //Setting up the view 
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, Display.getWidth(), Display.getHeight(), 0, zNear, zFar);
        GL11.glPushMatrix();
        //
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glPushMatrix();
    }
    /**
     * Initializes a standard Frustum View and sets the left-upper corner
     * as the coordinate root.
     */
    public static void initFrustumView(float fovy, float aspect, float zNear, float zFar) {
        //Setting up the view 
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GLU.gluPerspective(fovy, aspect, zNear, zFar);
        GL11.glPushMatrix();
        //
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glPushMatrix();
    }
    /**
     * Clears the screen with the clear color and loads the identity matrix.
     * <p/>
     * @param depthBuffer If the DepthBuffer should also be cleared.
     */
    public static void clearScreen(boolean depthBuffer) {
        // Clear the screen and depth buffer
        //GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT
        int mask = GL11.GL_COLOR_BUFFER_BIT;
        if (depthBuffer) {
            mask = mask | GL11.GL_DEPTH_BUFFER_BIT;
        }
        GL11.glClear(mask);
        //
        GL11.glLoadIdentity();
    }
    /**
     * Finds out all the DisplayModes that fit the default Screen Device and
     * support Fullscreen with LWJGL.
     * <p/>
     * @param minBitDepth The minimum amount of bits per pixel color required.
     * @return A List containing all the compatible DisplayModes.
     * @throws LWJGLException
     */
    public static List<GenericDisplayMode> getAllRealDisplayModes(int minBitDepth) throws LWJGLException {
        return getAllRealDisplayModes(minBitDepth, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice());
    }
    /**
     * Finds out all the DisplayModes that fit the specified GraphicsDevice and
     * support Fullscreen with LWJGL.
     * <p/>
     * @param minBitDepth The minimum amount of bits per pixel color required.
     * @return A List containing all the compatible DisplayModes.
     * @throws LWJGLException
     */
    public static List<GenericDisplayMode> getAllRealDisplayModes(int minBitDepth, GraphicsDevice screenDevice) throws LWJGLException {
        Set<GenericDisplayMode> modes = new TreeSet<>();
        //
        java.awt.DisplayMode[] awtModes = screenDevice.getDisplayModes();
        org.lwjgl.opengl.DisplayMode[] lwjglModes = Display.getAvailableDisplayModes();
        //
        GenericDisplayMode udm;
        for (org.lwjgl.opengl.DisplayMode mode : lwjglModes) {
            if (mode.isFullscreenCapable()) {
                udm = new GenericDisplayMode(mode);
                modes.add(udm);
            }
        }
        for (java.awt.DisplayMode mode : awtModes) {
            udm = new GenericDisplayMode(mode);
            modes.add(udm);
        }
        //
        List<GenericDisplayMode> modesList = new ArrayList<>();
        for (GenericDisplayMode mode : modes) {
            if (mode.getBitDepth() >= minBitDepth) {
                modesList.add(mode);
            }
        }
        return modesList;
    }
    /**
     * Get the Dimensions of the display.
     * <p/>
     * @return A Vektor representing the Display's dimension.
     */
    public static Vector2f getDisplayDimension() {
        return new Vector2f(Display.getWidth(), Display.getHeight());
    }
    /**
     * Get the AspectRatio of the screen.
     * @return The Display width divided by the Display Height.
     */
    public static float getAspectRatio(){
        return (float)Display.getWidth()/(float)Display.getHeight();
    }
    /**
     * Sets the current Color of the OpenGL instance.
     * @param color The new Color.
     */
    public static void applyColor(Color color) {
        float[] colors = color.getComponents(null);
        GL11.glColor4f(colors[0], colors[1],colors[2],colors[3]);
    }
    /**
     * Sets the clear Color used to clear the Display.
     * @param color The new Clear Color.
     */
    public static void setClearColor(Color color){
        float[] colors = color.getComponents(null);
        GL11.glClearColor(colors[0], colors[1],colors[2],colors[3]);
    }
}
