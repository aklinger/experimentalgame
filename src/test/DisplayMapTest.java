package test;

import experiment.code.camera.Kamera;
import experiment.code.main.GameLoop;
import experiment.code.map.CoolMap;
import experiment.code.math.Vector3D;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_LIGHTING;
import static org.lwjgl.opengl.GL11.glClear;

/**
 * A test to display a 3D map.
 * <p/>
 * @author Toni
 */
public class DisplayMapTest extends GameLoop {
    //Main
    public static void main(String[] args) {
        Runnable dmt = new DisplayMapTest();
        Thread t = new Thread(dmt);
        t.start();
    }
    //Attribute
    private CoolMap cmap;
    private Kamera kamera;
    private static final float mouseMoveSensitivity = 0.8f;
    private static final float mouseRotateSensitivity = 0.25f;
    private static final int DISPLAY_HEIGHT = 600;
    private static final int DISPLAY_WIDTH = 800;
    
    //Methoden
    @Override
    protected void initStuff() throws Exception {
        //Display
        Display.setDisplayMode(new DisplayMode(DISPLAY_WIDTH, DISPLAY_HEIGHT));
        Display.setFullscreen(false);
        Display.setTitle("Hello Map Render Test!");
        Display.create();

        //Keyboard
        Keyboard.create();

        //Mouse
        Mouse.setGrabbed(false);
        Mouse.create();

        //OpenGL
        initGL();
        initOrthoView();

        cmap = CoolMap.createCube(7, 3, 20, 0.9);
        kamera = new Kamera();
        kamera.addPosition(new Vector3D(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2, 0));
    }

    public void initGL() {
        //2D Initialization
        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GL11.glEnable(GL_DEPTH_TEST);
        GL11.glDisable(GL_LIGHTING);
    }

    /**
     * Initializes a standard Orthographic View and sets the left-upper corner
     * as the coordinate root.
     */
    public static void initOrthoView() {
        //Setting up the view 
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, Display.getWidth(), Display.getHeight(), 0, -1000, 1000);
        GL11.glPushMatrix();
        //
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glPushMatrix();
    }

    @Override
    public void act() {
        //distance in mouse movement from the last getDX() call.
        int dx = Mouse.getDX();
        //distance in mouse movement from the last getDY() call.
        int dy = Mouse.getDY();
        if (Mouse.isButtonDown(1)) {
            //control camera yaw from x movement from the mouse
            kamera.setYaw(kamera.getYaw() + dx * mouseRotateSensitivity);
            //control camera pitch from y movement from the mouse
            kamera.setPitch(kamera.getPitch() + dy * mouseRotateSensitivity);
        }
        if (Mouse.isButtonDown(0)) {
            kamera.addPosition(new Vector3D(dx * mouseMoveSensitivity, -dy * mouseMoveSensitivity, 0));
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
            this.quit();
        }
    }

    @Override
    public void render() {
        glClear(GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        //set the modelview matrix back to the identity
        GL11.glLoadIdentity();
        //look through the camera before you draw anything
        kamera.lookThrough();
        //you would draw your scene here.	

        //GL11.glScaled(0.5, 0.5, 0.5);

        cmap.render();
    }

    @Override
    protected void cleanup() {
        Mouse.destroy();
        Keyboard.destroy();
        Display.destroy();
    }
}
