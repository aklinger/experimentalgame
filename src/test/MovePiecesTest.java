package test;

import experiment.code.gl.Light;
import experiment.code.logic.Manager;
import experiment.code.logic.ManagerFactory;
import experiment.code.main.GameLoop;
import experiment.code.util.DisplayUtil;
import java.awt.Color;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

/**
 *
 * @author Toni
 */
public class MovePiecesTest extends GameLoop{
    //Main
    public static void main(String[] args) {
        MovePiecesTest mpt = new MovePiecesTest();
        mpt.setEscapeTime(5);
        Thread t = new Thread(mpt);
        t.start();
    }
    //Attribute
    private Manager manager;
    //Methoden
    @Override
    protected void initStuff() throws Exception {
        DisplayUtil.initWindow(Display.getDesktopDisplayMode(),"Hello Pieces Test!",false);
        
        //Keyboard
        Keyboard.create();

        //Mouse
        Mouse.setGrabbed(false);
        Mouse.create();

        //OpenGL
        DisplayUtil.initOpenGL(Color.DARK_GRAY, true, true, true, true);
        DisplayUtil.initFrustumView(45f, DisplayUtil.getAspectRatio(), 10, 1000);
        Light licht = new Light();
        licht.initOpenGLLighting();
        
        //Game
        manager = ManagerFactory.createManager(ManagerFactory.CHESS,false);
        
    }

    @Override
    public void act() {
        manager.act();
    }

    @Override
    public void render() {
        DisplayUtil.clearScreen(true);
        manager.render();
    }

    @Override
    protected void cleanup() {
        Mouse.destroy();
        Keyboard.destroy();
        Display.destroy();
    }
    //Getter & Setter
    
}
