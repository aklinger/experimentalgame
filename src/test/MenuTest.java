package test;

import experiment.code.gl.Light;
import experiment.code.logic.Manager;
import experiment.code.logic.ManagerFactory;
import experiment.code.main.GameLoop;
import experiment.code.util.DisplayUtil;
import java.awt.Color;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

/**
 *
 * @author Toni
 */
public class MenuTest extends GameLoop{
    //Main
    public static void main(String[] args) {
        MenuTest mpt = new MenuTest();
        mpt.setEscapeTime(5);
        Thread t = new Thread(mpt);
        t.start();
    }
    //Attribute
    private Manager manager;
    //Methoden
    @Override
    protected void initStuff() throws Exception {
        DisplayUtil.initWindow(Display.getDesktopDisplayMode(),"Mini LD 44 (7dRTS) - Anton Klinger",false);
        
        //Keyboard
        Keyboard.create();

        //Mouse
        Mouse.setGrabbed(false);
        Mouse.create();

        //OpenGL
        DisplayUtil.initOpenGL(Color.DARK_GRAY, true, true, true, true);
        DisplayUtil.initFrustumView(45f, DisplayUtil.getAspectRatio(), 10, 1000);
        Light licht = new Light();
        licht.initOpenGLLighting();
        
        //Game
        manager = ManagerFactory.createManager(ManagerFactory.SPLASH,false);
        
    }

    @Override
    public void act() {
        try {
            if(Keyboard.isKeyDown(Keyboard.KEY_1) ||
                    Keyboard.isKeyDown(Keyboard.KEY_NUMPAD1)){
                manager = ManagerFactory.createManager(ManagerFactory.MENU,true);
            }else if(Keyboard.isKeyDown(Keyboard.KEY_2) ||
                    Keyboard.isKeyDown(Keyboard.KEY_NUMPAD2)){
                manager = ManagerFactory.createManager(ManagerFactory.SMALL,false);
            }else if(Keyboard.isKeyDown(Keyboard.KEY_6) ||
                    Keyboard.isKeyDown(Keyboard.KEY_NUMPAD6)){
                manager = ManagerFactory.createManager(ManagerFactory.SMALL,true);
            }else if(Keyboard.isKeyDown(Keyboard.KEY_3) ||
                    Keyboard.isKeyDown(Keyboard.KEY_NUMPAD3)){
                manager = ManagerFactory.createManager(ManagerFactory.BLOCK,false);
            }else if(Keyboard.isKeyDown(Keyboard.KEY_7) ||
                    Keyboard.isKeyDown(Keyboard.KEY_NUMPAD7)){
                manager = ManagerFactory.createManager(ManagerFactory.BLOCK,true);
            }else if(Keyboard.isKeyDown(Keyboard.KEY_4) ||
                    Keyboard.isKeyDown(Keyboard.KEY_NUMPAD4)){
                manager = ManagerFactory.createManager(ManagerFactory.CRAZY,false);
            }else if(Keyboard.isKeyDown(Keyboard.KEY_8) ||
                    Keyboard.isKeyDown(Keyboard.KEY_NUMPAD8)){
                manager = ManagerFactory.createManager(ManagerFactory.CRAZY,true);
            }else if(Keyboard.isKeyDown(Keyboard.KEY_5) ||
                    Keyboard.isKeyDown(Keyboard.KEY_NUMPAD5)){
                manager = ManagerFactory.createManager(ManagerFactory.CHESS,false);
            }else if(Keyboard.isKeyDown(Keyboard.KEY_9) ||
                    Keyboard.isKeyDown(Keyboard.KEY_NUMPAD9)){
                manager = ManagerFactory.createManager(ManagerFactory.CHESS,true);
            }else if(Keyboard.isKeyDown(Keyboard.KEY_A)){
                manager = ManagerFactory.createManager(ManagerFactory.ARIMAA,false);
            }else if(Keyboard.isKeyDown(Keyboard.KEY_T) ||
                    Keyboard.isKeyDown(Keyboard.KEY_NUMPAD0)){
                manager = ManagerFactory.createManager(ManagerFactory.TUTORIAL,false);
            }
        } catch (IOException ex) {
            Logger.getLogger(MenuTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        manager.act();
    }

    @Override
    public void render() {
        DisplayUtil.clearScreen(true);
        manager.render();
    }

    @Override
    protected void cleanup() {
        Mouse.destroy();
        Keyboard.destroy();
        Display.destroy();
    }
    //Getter & Setter
    
}
