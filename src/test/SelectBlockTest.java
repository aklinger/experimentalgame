package test;

import experiment.code.util.DisplayUtil;
import experiment.code.camera.QuaternionCamera;
import experiment.code.gl.Light;
import experiment.code.main.GameLoop;
import experiment.code.map.CoolMap;
import experiment.code.map.CubeFace;
import experiment.code.math.Ray;
import experiment.code.model.Model;
import experiment.code.model.OBJLoader;
import experiment.code.model.Piece;
import java.awt.Color;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayDeque;
import java.util.Collection;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnd;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector3f;

/**
 * This test is for trying out how to select a block/cube in a 3D environment
 * via mouse.
 * <p/>
 * @author Toni
 */
public class SelectBlockTest extends GameLoop {
    //Main
    public static void main(String[] args) {
        SelectBlockTest sbt = new SelectBlockTest();
        sbt.setEscapeTime(10);
        Thread t = new Thread(sbt);
        t.start();
    }
    //Attribute
    private static final float mouseMoveSensitivity = 0.05f;
    private static final float mouseRotateSensitivity = 0.01f;
    private static final float zoomSpeed = 2.2f;
    private static final float zNear = 10;
    private static final float zFar = 1000;
    private CoolMap map;
    private QuaternionCamera cam;
    private CubeFace selectedFace;
    private Collection<Ray> rays;
    private Piece piece;
    private CubeFace piecePos;
    private Light licht;
    //Methoden
    @Override
    protected void initStuff() throws Exception {
        boolean debug = true;
        //Display
        DisplayMode mode = Display.getDesktopDisplayMode();
        if(debug){
            mode = new DisplayMode(800, 600);
        }
        DisplayUtil.initWindow(mode, "Hello Select Block HitTest!", true);

        //Keyboard
        Keyboard.create();

        //Mouse
        Mouse.setGrabbed(false);
        Mouse.create();

        //OpenGL
        DisplayUtil.initOpenGL(Color.DARK_GRAY, true, true, true, true);
        DisplayUtil.initFrustumView(45f, DisplayUtil.getAspectRatio(), zNear, zFar);
        licht = new Light();
        licht.initOpenGLLighting();

        map = CoolMap.createCube(10, 8, 2, 0.5);
        cam = new QuaternionCamera();
        cam.setZoomLimits(0.1f, 10f);
        cam.addTranslation(new Vector3f(0, 0, -50));

        rays = new ArrayDeque<>();
        
        Model m = OBJLoader.loadModel("/res/models/chess/chess_piece1.obj");
        m.createModelDisplayList();
        piece = new Piece(m,0.35f);
    }

    @Override
    public void act() {
        //distance in mouse movement from the last getDX() call.
        int dx = Mouse.getDX();
        //distance in mouse movement from the last getDY() call.
        int dy = -Mouse.getDY();
        if (Mouse.isButtonDown(2) || Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
            cam.addTranslation(new Vector3f(dx * mouseMoveSensitivity, -dy * mouseMoveSensitivity, 0));
        }
        if (Mouse.isButtonDown(1)) {
            cam.rotateQuaternionCamera(QuaternionCamera.X_AXIS, dy * mouseRotateSensitivity);
            cam.rotateQuaternionCamera(QuaternionCamera.Y_AXIS, dx * mouseRotateSensitivity);
            cam.updateQuaternionCamera();
        }
        int wheel = Mouse.getDWheel();
        if (wheel != 0) {
            wheel /= 120;
            cam.addZoom(zoomSpeed * wheel);
            //cam.addTranslation(new Vector3f(0, 0, wheel * zoomSpeed));
        }
        if (Mouse.isButtonDown(0)) {
            Ray r = Ray.getPickRay();
            rays.add(r);
            selectedFace = map.getFacePickedByRay(r);
            if(selectedFace != null){
                piecePos = selectedFace;
            }
        }
    }

    @Override
    public void render() {
        DisplayUtil.clearScreen(true);

        cam.lookThrough();

        map.render();

        GL11.glPushMatrix();
        glColor3f(0.2f, 0.8f, 0.9f);
        for (Ray currentRay : rays) {
            float length = 1000;
            glBegin(GL11.GL_LINE_LOOP);
            GL11.glVertex3f(currentRay.getPosition().x, currentRay.getPosition().y, currentRay.getPosition().z);
            GL11.glVertex3f(currentRay.getPosition().x + currentRay.getDirection().x * length, currentRay.getPosition().y + currentRay.getDirection().y * length, currentRay.getPosition().z + currentRay.getDirection().z * length);
            glEnd();
        }
        GL11.glPopMatrix();
        
        if(selectedFace != null){
            glColor3f(1f,1f,1f);
            map.renderFace(selectedFace);
        }
        
        if(piecePos != null){
            piece.renderPiece(piecePos, null, map);
        }
    }

    @Override
    protected void cleanup() {
        Mouse.destroy();
        Keyboard.destroy();
        Display.destroy();
    }
}
