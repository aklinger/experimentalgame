package test;

import experiment.code.camera.Kamera;
import experiment.code.main.GameLoop;
import experiment.code.map.CoolMap;
import experiment.code.math.Vector3D;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_LIGHTING;
import static org.lwjgl.opengl.GL11.glClear;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector4f;
import static test.DisplayMapTest.initOrthoView;

/**
 *
 * @author Toni
 */
public class QuaternionTest extends GameLoop {
    //Main
    public static void main(String[] args) {
        Runnable qt = new QuaternionTest();
        Thread t = new Thread(qt);
        t.start();
    }
    //Attribute
    private CoolMap cmap;
    private Kamera kamera;
    private FloatBuffer matrixBuffer;
    private Quaternion quatCurrentRotation;
    private Quaternion quatRotationChange;
    
    private float zoom = 1;
    private float zoomSpeed = 1.2f;
    private float zoomMin = 0.1f;
    private float zoomMax = 10f;
    
    private static final float mouseMoveSensitivity = 0.8f;
    private static final float mouseRotateSensitivity = 0.01f;
    private static final int DISPLAY_HEIGHT = 600;
    private static final int DISPLAY_WIDTH = 800;

    //Methoden
    @Override
    protected void initStuff() throws Exception {
        //Display
        Display.setDisplayMode(new DisplayMode(DISPLAY_WIDTH, DISPLAY_HEIGHT));
        Display.setFullscreen(false);
        Display.setTitle("Hello Quaternion Camera Render Test!");
        Display.create();

        //Keyboard
        Keyboard.create();

        //Mouse
        Mouse.setGrabbed(false);
        Mouse.create();

        //OpenGL
        initGL();
        initOrthoView();

        cmap = CoolMap.createCube(11, 8, 20, 0.1);
        kamera = new Kamera();
        kamera.addPosition(new Vector3D(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2, -300));

        matrixBuffer = BufferUtils.createFloatBuffer(16);
        quatCurrentRotation = new Quaternion();
        quatRotationChange = new Quaternion();
    }

    public void initGL() {
        //2D Initialization
        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GL11.glEnable(GL_DEPTH_TEST);
        GL11.glDisable(GL_LIGHTING);
    }

    /**
     * Initializes a standard Orthographic View and sets the left-upper corner
     * as the coordinate root.
     */
    public static void initOrthoView() {
        //Setting up the view 
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, Display.getWidth(), Display.getHeight(), 0, -10, 1000);
        GL11.glPushMatrix();
        //
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glPushMatrix();
    }

    @Override
    public void act() {
        //distance in mouse movement from the last getDX() call.
        int dx = Mouse.getDX();
        //distance in mouse movement from the last getDY() call.
        int dy = Mouse.getDY();
        if (Mouse.isButtonDown(0)) {
            kamera.addPosition(new Vector3D(dx * mouseMoveSensitivity, -dy * mouseMoveSensitivity, 0));
        }
        if (Mouse.isButtonDown(1)) {
            Vector4f rotVect = new Vector4f(1, 0, 0, dy * mouseRotateSensitivity);
            rotateQuaternionCamera(rotVect);
            rotVect = new Vector4f(0, 1, 0, dx * mouseRotateSensitivity);
            rotateQuaternionCamera(rotVect);
        }
        int wheel = Mouse.getDWheel();
        if (wheel != 0) {
            wheel /= 120;
            if (wheel > 0) {
                zoom *= zoomSpeed * wheel;
            } else {
                zoom /= zoomSpeed * -wheel;
            }
            if (zoom > zoomMax) {
                zoom = zoomMax;
            } else if (zoom < zoomMin) {
                zoom = zoomMin;
            }
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
            this.quit();
        }
    }

    @Override
    public void render() {
        glClear(GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        //set the modelview matrix back to the identity
        GL11.glLoadIdentity();
        //look through the camera before you draw anything
        kamera.lookThrough();
        applyQuaternionCamera();

        //you would draw your scene here.

        GL11.glScaled(zoom, zoom, zoom);

        cmap.render();
    }

    @Override
    protected void cleanup() {
        Mouse.destroy();
        Keyboard.destroy();
        Display.destroy();
    }

    public void rotateQuaternionCamera(Vector4f rotationVector) {
        quatRotationChange.setFromAxisAngle(rotationVector);
        Quaternion.mul(quatRotationChange, quatCurrentRotation, quatCurrentRotation);
        quatCurrentRotation.normalise();
    }

    public void applyQuaternionCamera() {
        createMatrixBuffer();
        GL11.glMultMatrix(matrixBuffer);
    }

    /**
     * http://www.java-gaming.org/index.php?topic=24177.0
     * http://www.genesis3d.com/~kdtop/Quaternions-UsingToRepresentRotation.htm
     */
    private void createMatrixBuffer() {
        float w, x, y, z;
        w = quatCurrentRotation.w;
        x = quatCurrentRotation.x;
        y = quatCurrentRotation.y;
        z = quatCurrentRotation.z;
        Matrix4f m = new Matrix4f();

        m.m00 = sq(w) + sq(x) - sq(y) - sq(z);
        m.m01 = 2 * x * y + 2 * w * z;
        m.m02 = 2 * x * z - 2 * w * y;
        m.m03 = 0;

        m.m10 = 2 * x * y - 2 * w * z;
        m.m11 = sq(w) - sq(x) + sq(y) - sq(z);
        m.m12 = 2 * y * z + 2 * w * x;
        m.m13 = 0;

        m.m20 = 2 * x * z + 2 * w * y;
        m.m21 = 2 * y * z - 2 * w * x;
        m.m22 = sq(w) - sq(x) - sq(y) + sq(z);
        m.m23 = 0;

        m.m30 = 0;
        m.m31 = 0;
        m.m32 = 0;
        m.m33 = sq(w) + sq(x) + sq(y) + sq(z);

        matrixBuffer.clear();
        m.store(matrixBuffer);
        matrixBuffer.flip();
    }

    private float sq(float x) {
        return x * x;
    }
}
